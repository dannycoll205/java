package lecture07;

public class HighBalanceException extends Exception {
	public HighBalanceException() {
		super("Customer balance is high");
	}
}