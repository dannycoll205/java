package lecture07;

public class CustomerAccount {
	private int accNum;
	private double balance;
	public static double HIGH_CREDIT_LIMIT = 20000.00;
	public static double LOW_LIMIT = 0;
	
	public CustomerAccount(int num, double bal) throws HighBalanceException {
		this.accNum = num;
		balance = bal;
		if (balance > HIGH_CREDIT_LIMIT) {
			throw(new HighBalanceException());
		}
	}
	
	public void remove_money(double deduct) throws LowBalanceException{
		if(this.balance-deduct<LOW_LIMIT) {
			throw(new LowBalanceException());
		}
	}
}
