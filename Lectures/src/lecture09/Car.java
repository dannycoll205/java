package lecture09;

public class Car extends Vehicle {
	private String regNum;
	public Car(int numTyres, double size, String colour, String regNum) {
		super(numTyres, size, colour);
		this.regNum=regNum;
	}
	
	public String getRegNum() {
		return regNum;
	}
	public void setRegNum(String regNum) {
		this.regNum = regNum;
	}
	
	public String getDetails() {
		return "Car Description:\n"+printDetails()+"\nRegistration NUmber: "+getRegNum();
	}

}
