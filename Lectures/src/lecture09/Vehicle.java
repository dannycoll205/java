package lecture09;

public class Vehicle {
	private int numTyres;
	private double engineSize;
	private String colour;
	
	public Vehicle(int numTyres, double size, String colour) {
		this.numTyres=numTyres;
		this.engineSize=size;
		this.colour=colour;
	}
	
	
	
	public int getNumTyres() {
		return numTyres;
	}



	public void setNumTyres(int numTyres) {
		this.numTyres = numTyres;
	}



	public double getEngineSize() {
		return engineSize;
	}

	public void setEngineSize(double engineSize) {
		this.engineSize = engineSize;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	public String printDetails() {
		return "Number of tyres: "+numTyres+"\nEngine Size: "+engineSize+"\nColour: "+colour;
	}
	
	public String getDetails() {
		return "Vehicle Description:\n"+printDetails();
	}
}
