package lecture09;

public class InheritTester {

	public static void main(String[] args) {
		Vehicle mazda=new Vehicle(4,1.6,"Red");
		print(mazda);
		
		Car vw=new Car(2,1.8,"Yellow", "ABC123");
		print(vw);
		
		if(mazda instanceof Car) {
			System.out.println("True");
		}else {
			System.out.println("False");
		}
		
		System.out.println("Is vw an instance of Vehicle? "+(vw instanceof Vehicle));
	}
	
	public static void print(Vehicle v) {
		System.out.println(v.getDetails());
	}
}
