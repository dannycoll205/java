package lecture09;

public class Recursion {

	public static void main(String[] args) {
		
		System.out.println(sequence(5));
	}
	
	public static int sequence(int k) {
		if(k==0) {
			return 20;
		}else {
			
			return (sequence(k-1)-6);
			
		}
	}
}
