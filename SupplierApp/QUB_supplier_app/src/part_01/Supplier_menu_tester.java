package part_01;

import java.util.ArrayList;
import java.util.Scanner;

public class Supplier_menu_tester {
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		ArrayList<Product> supplies = new ArrayList<Product>(); 
		supplies.add(new Product(40203,"DM","Basic Paper Ream",5.99,10,false));
		supplies.add(new Product(40203,"DM","Premium Paper Ream",7.99,10,false));
		Supplier dm = new Supplier(0, "Dunder Mifflin", new Address(1725, "Slough Avenue", "Scanton", "PA18505", "USA"), SupRegion.OUTSIDE_EU, supplies);
		
		String options[] = { "1. Print All Products", "2. Add New Supplier", "3. Add New Product", "4. Exit Application"};
		
		ArrayList<Supplier> all_suppliers = new ArrayList<Supplier>();
		all_suppliers.add(dm);
		
		boolean finished = false;		
		do {
			displayMenu(options);
			int choice = getUserChoice();
			switch(choice) {
			case 1	:	System.out.println("Option 1 selected");
						break;
			case 2	:	System.out.println("Option 2 selected");
						break;
			case 3	:	System.out.println("Option 3 selected");
						break;
			case 4	:	System.out.println("Option 4 selected");
						finished = true;
						break;
			default	:	System.out.println("Error - Invalid Choice");
			}
		}
		while( !finished );
		System.out.println("Goodbye!");
	}
	
	static int getUserChoice() {
		System.out.print("Enter choice:");
		while( !input.hasNextInt() ) {
			input.nextLine();
			System.out.print("Enter choice:");
		}
		int choice = input.nextInt();
		input.nextLine();
		return choice;
	}
	static void displayMenu(String[] options){
		System.out.println("Main Menu");
		System.out.println("=========\n");
		
		for ( String str : options) {
			System.out.println( str );
		}
		System.out.println();
	}
}
