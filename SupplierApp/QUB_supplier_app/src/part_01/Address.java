package part_01;

public class Address {
	private int bldNum;
	private String bldStreet;
	private String bldTown;
	private String bldPcode;
	private String bldCountry;
	
	public Address(int bldNum, String bldStreet, String bldTown,String bldPcode, String bldCountry) {
		this.bldNum=bldNum;
		this.bldStreet = bldStreet;
		this.bldTown = bldTown;
		this.bldPcode = bldPcode;
		this.bldCountry = bldCountry;
	}
	
	public String getFullAddress() {
		return null;
	}
	public int getBldNum() {
		return this.bldNum;
	}
	public String getBldStreet() {
		return this.bldStreet;
	}
	public String getBldTown() {
		return this.bldTown;
	}
	public String getBldPcode() {
		return this.bldPcode;
	}
	public String getBldCountry() {
		return this.bldCountry;
	}
	public void setBldNum(int bldNum) {
		this.bldNum=bldNum;
	}
	public void setBldStreet(String bldStreet) {
		this.bldStreet=bldStreet;
	}
	public void setBldTown(String bldTown) {
		this.bldTown = bldTown;
	}
	public void setBldPcode(String bldPostcode) {
		this.bldPcode=bldPostcode;
	}
	public void setBldCountr(String bldCountry) {
		this.bldCountry=bldCountry;
	}

}
