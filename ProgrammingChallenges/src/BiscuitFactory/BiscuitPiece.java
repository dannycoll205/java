package BiscuitFactory;

public class BiscuitPiece {
	double weight;
	
	BiscuitPiece(double weight){
		this.weight = weight;
	}
	
	public double getWeight() {
		return weight;
	}
	public void setWeight(double newW){
		weight = newW;
		
	}
}
