package Tasks;

import java.util.Scanner;

public class QuestionTester {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); 
		 
		Question quOne = new Question ("What is the module code for Fundamentals of Programming?", "CSC1021", 2); 
		quOne.displayQuestion(); 
		 
		String userAnswer = input.nextLine(); 
		 
		boolean userCorrect = quOne.checkAnswer(userAnswer); 
		 
		if (userCorrect) { 
			System.out.println("Correct! The answer is "   + quOne.getAnswer() + " you have a score of " + quOne.getMark()); 
		} else { 
			System.out.println(userAnswer + " is an incorrect answer.  You scored zero."); 
		} 
		
		quOne.setQuText("What is the code for Programming?");
		quOne.setAnswer("CSC1020");
		
		quOne.displayQuestion();
		input.close();

	}

}
