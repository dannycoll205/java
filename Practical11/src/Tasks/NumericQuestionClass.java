package Tasks;

public class NumericQuestionClass extends Question {
	
	private double ansNumeric; 
	private double positiveRange; 
	private double negativeRange;

	public NumericQuestionClass(String quText, String answer, int mark, double posRange, double negRange) {
		super(quText, answer, mark);
		//try {
			//this.ansNumeric=Double.parseDouble(answer); 
		//}catch(NumberFormatException e) {
			System.out.println("Input must be number.");
		//}
		this.positiveRange=posRange; 
		this.negativeRange=negRange;
		
	}

	public double getAnsNumeric() {
		return ansNumeric;
	}

	public void setAnsNumeric(double ansNumeric) {
		this.ansNumeric = ansNumeric;
	}
	
	public boolean checkAnswer(String response) {  
		double responseDouble = Double.parseDouble(response); 
		if(responseDouble>(this.ansNumeric+this.positiveRange)) {  
			return false; 
		} else if(responseDouble<(this.ansNumeric-this.negativeRange)) { 
			return false; 
		} else {  
			return true; 
		}
	} 

}
