package Tasks;

public class Question {
	private String quText;
	private String answer;
	private int mark;
	
	
	public Question(String quText, String answer, int mark) {
		this.quText = quText;
		this.answer = answer;
		this.mark = mark;
	}
	public String getQuText() {
		return quText;
	}
	public void setQuText(String quText) {
		this.quText = quText;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public int getMark() {
		return mark;
	}
	public void setMark(int mark) {
		this.mark = mark;
	}
	
	public boolean checkAnswer(String userResponse) {
		return this.answer.equalsIgnoreCase(userResponse);
	} 
	 
	public void displayQuestion() { 
		System.out.println(this.quText);
	} 
}
