package Tasks;

import java.util.ArrayList;

public class ChoiceQuestion extends Question {
	
	private ArrayList<String> choices;

	public ChoiceQuestion(String quText, String answer, int mark, ArrayList<String> choices) {
		super(quText, answer, mark);
		this.choices = new ArrayList<String>(choices);
	}
	
	public void displayQuestion() { 
		super.displayQuestion(); 
		for (int i = 0; i < this.choices.size(); i++) {  
			int choiceNumber = i + 1; 
			System.out.println(choiceNumber + ": " + this.choices.get(i)); 
		}
	}

	public ArrayList<String> getChoices() {
		return choices;
	}

	public void setChoices(ArrayList<String> choices) {
		this.choices = new ArrayList<String>(choices);
	}
	
	

}
