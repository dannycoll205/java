package Tasks;

import java.util.ArrayList;
import java.util.Scanner;

public class QuizTester {
	
	public static int quizTotal = 0;
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		Question quOne = new Question("What is the module code for Fundamentals of Programming?", "CSC1021",2);
		
		ArrayList<String> ansChoices = new ArrayList<String>(); 
		
		ansChoices.add("10%");
		ansChoices.add("20%"); 
		ansChoices.add("30%"); 
		ansChoices.add("40%");
		
		ChoiceQuestion quTwo = new ChoiceQuestion("What percentage of the assessment for CSC1020 has been completed to date?","4",2,ansChoices);
		
		NumericQuestionClass quThree = new NumericQuestionClass("Provide the answer to 5 divided by 3?","1.67",2,0.03,0.07);
		
		presentQuestion(input,quOne);
		presentQuestion(input, quTwo);
		presentQuestion(input, quThree); 
		
		input.close();

	}
	
	 public static void presentQuestion(Scanner in, Question q) {  
		 q.displayQuestion();  
		 System.out.print("Your answer: ");  
		 String response = in.nextLine();
		 if (q.checkAnswer(response)) {   
			 System.out.println("Correct! The answer is " + q.getAnswer() + " you have a score of " + q.getMark()); 
			 quizTotal += q.getMark();
		 } else {  
			 System.out.println(response + " is an incorrect answer.  You scored zero.");  
		 }
		 
		 System.out.println(); 
		 System.out.println("You total score for the quiz is " + quizTotal);
	 } 

}
