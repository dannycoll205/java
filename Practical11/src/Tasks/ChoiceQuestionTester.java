package Tasks;

import java.util.ArrayList;
import java.util.Scanner;

public class ChoiceQuestionTester {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		ArrayList<String> ansChoices = new ArrayList<String>(); 
		
		ansChoices.add("10%");
		ansChoices.add("20%"); 
		ansChoices.add("30%"); 
		ansChoices.add("40%");
		
		ChoiceQuestion quTwo = new ChoiceQuestion("What percentage of the assessment for CSC1020 has been completed to date?","4",2,ansChoices);
		
		presentQuestion(input, quTwo);
		input.close();

	}
	
	 public static void presentQuestion(Scanner in, ChoiceQuestion q) {  
		 q.displayQuestion();  
		 System.out.print("Your answer: ");  
		 String response = in.nextLine();
		 if (q.checkAnswer(response)) {   
			 System.out.println("Correct! The answer is " + q.getAnswer() + " you have a score of " + q.getMark());  
		 } else {  
			 System.out.println(response + " is an incorrect answer.  You scored zero.");  
		 }
		 
		 System.out.println(); 
	 } 
}
