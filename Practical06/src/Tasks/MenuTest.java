package Tasks;

import java.util.ArrayList;
import java.util.Scanner;

public class MenuTest {
	static String option[]  = {"1. Create Student",
			"2. Add Module",
			"3. Display a Student",
			"4. Display All Students",
			"5. Quit"};
	static Scanner input = new Scanner(System.in);
	public static void main(String[] args) {
		
		Student stu = null;
		ArrayList <Student> college = new ArrayList<Student>();
		
		
		boolean finished = false;
		do {//display menu choices
			displayMenu();
			System.out.println("Select option");			
			//ask user to pick 
			char choice = getUserChoice();
			//act on choice
			//one will be quit
			switch(choice) {
			case '1': stu = createStudent();
					college.add(stu);
					break;
			case '2': String mod = getModule();
					if(stu != null) {
						stu.addModule(mod);
					}else {
						System.out.println("Please add a Student.");
					}
					break;
			case '3': //pick student
					stu = selectStudent(college);
					//display student
					if(stu != null) {
						System.out.println(stu);
					}else {
						System.out.println("Error no Students exist.");
					}
					break;	
			case '4': if(stu != null) {
						displayAllStudents(college);
					}else {
						System.out.println("No Student Exists.");
					}
					break;
			case '5': finished = true;
					break;
			default: System.out.println("ERROR: Invalid Choice");			
			}
			
		}while(!finished);
		System.out.println("Goodbye!");
		}
	static void displayMenu() {
		for(int i = 0; i < option.length; i ++) {
			System.out.println(option[i]);
		}
	}
	
	static char getUserChoice() {
		System.out.print("Enter Choice:");
		String str = input.next();
		char ch = str.charAt(0);
		return ch;
	}
	
	static Student createStudent() {
		System.out.print("Enter name:");
		String name = input.next();
		
		System.out.print("Enter number:");
		String number = input.next();
		
		Student s = new Student(number, name);
		
		return s;
	}
	
	static String getModule() {
		System.out.print("Enter Module:");
		String module = input.next();
		return module;
	}
	
	static void displayAllStudents(ArrayList<Student> all) {

		System.out.println("====================");
		for(int i = 0; i<all.size(); i++) {
			Student s = all.get(i);
			System.out.println((i+1) + ": " +s.getNumber() + " " + s.getName());
		}
		System.out.println();
	}
	
	static int whichStudent() {
		System.out.println("Select Student: ");
		int num = input.nextInt();
		return num;
	}
	static Student selectStudent(ArrayList<Student> all) {
		displayAllStudents(all);
		int choice = whichStudent();
		return all.get(choice-1);
	}
}
