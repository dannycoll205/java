package Tasks;

import java.util.Scanner;

public class GuessNumberGame {

	public static void main(String[] args) {
		int number = 5;
		int userInput;
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Guess the number:");
		
		userInput = input.nextInt();
		
		while(userInput != number) {
			System.out.println("Wrong, try again:");
			
			userInput = input.nextInt();
		}
		
		System.out.println("Well done! You guessed it!");
		

	}

}
