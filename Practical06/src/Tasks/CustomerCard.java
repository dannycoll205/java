package Tasks;

public class CustomerCard {
	private String name, postcode, mobile, date, cardNumber;
	private int numberOfShops, currentPoints, pointsToDeduct, randomNum;
	private double totalSpend, avgSpend;
	
	public CustomerCard(String name, String postcode, String mobile, String date) {
		this.name = name;
		this.postcode = postcode;
		this.mobile = mobile;
		this.date = date;
		cardNumber = getCardNumber();
		numberOfShops = 0;
		currentPoints = 500;
		totalSpend = 0;
		
	}
	
	public String getCardNumber() {
		cardNumber = "";
		
		for(int index = 1; index <= 5; index ++) {
			randomNum = (int)(Math.random()*9);
			cardNumber += randomNum;
		}
		
		cardNumber += name.charAt(0);
		return cardNumber;
	}
	
	public String printDetails() {
		String details = "";
		details += "Name: \t\t" + name +"\n";
		details += "Postcode: \t" + postcode +"\n";
		details += "Mobile: \t" + mobile +"\n";
		details += "Date Joined: \t" + date +"\n";
		details += "Card No.: \t" + cardNumber + "\n";
		details += "Total Shops: \t" + numberOfShops +"\n";
		details += "Total Spend: \t" + totalSpend +"\n";
		details += "Average Spend: \t" + avgSpend + "\n";
		details += "Total Points: \t" + currentPoints +"\n";
		
		return details;
	}
}
