package Tasks;

public class SumAndAverageWhile { 
	public static void main(String[] args) { 
		int sum = 0;  
		double average; 
		int upperbound = 100; 
		int number = 1;
		while(number <= upperbound) {
			sum += number;
			number ++;
			System.out.println("Current number: " + number + " the sum is " + sum);
		}
		// Compute average in double. Beware that int/int produces int.  
		average = (double)sum / upperbound;
		// Print sum and average. 
		System.out.println("Sum: \t" + sum);
		System.out.printf("Average:%.2f", average);
		
		}
	
	}

