package Tasks;

import java.util.Scanner;

public class MinAndMax {

	public static void main(String[] args) {
		int userInput = 0;
		int min, max;
		String userInputTemp;
		Scanner input = new Scanner(System.in);
		
		System.out.println("Please input a value: ");
		userInput = input.nextInt();
		
		
		
		
		max = userInput;
		min = userInput;
		
		for (int count = 1; count < 10; count ++) {
			System.out.println("Please input another value: ");
			userInput = input.nextInt();
			if(userInput < min) {
				min = userInput;
			}else if(userInput > max) {
				max = userInput;
			}
		}
		
		System.out.println("The max input was: " + max + " while the min was: " + min);

	}

}
