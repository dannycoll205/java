package Tasks;

import java.util.Scanner;

public class NumberPrinter {

	public static void main(String[] args) {
		int n = 0;
		Scanner input = new Scanner(System.in);
		boolean invalid = true;
		do{
			System.out.println("Input a number: ");	
			if(!input.hasNextInt()) {
				System.out.println("ERROR");
				input.nextLine();
			}else {
				invalid = false;
				n = input.nextInt();
			}
		}
		while(invalid); 
		
		for(int i = 1; i <= n; i ++) {
			System.out.print(i + " ");
		}

	}

}
