package Tasks;

import java.util.Scanner;

public class StudentTester {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Student s1 = new Student("Paul", "123");
		
		do {
			System.out.println("Module?");
			String mod = input.next();
			if(mod.equalsIgnoreCase("quit")) {
				break;
			}
			
			boolean ok = s1.addModule(mod);
			if(ok) {
				System.out.println("Added " + mod);
			}else {
				System.out.println("Can't add " + mod);
				break;
			}
		}while(true);
		
		System.out.println(s1.toString());
		
		System.out.println("Bye");


	}

}
