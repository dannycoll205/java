package Tasks;

public class Student {
	private String number, name, courses[];
	private final int MAX_COURSES = 3;
	private int enrolCount;
	
	public Student(String name, String num) {
		this.number = num;
		this.name = name;
		enrolCount = 0;
		courses = new String[MAX_COURSES];
	}
	
	public String getName() {
		return name;
	}
	
	public String getNumber() {
		return number;
	}
	public boolean addModule(String name) {
		if(enrolCount < MAX_COURSES) {
			courses[enrolCount] = name;
			enrolCount ++;
			return true;
		}
		
		return false;
	}
	
	public String studentDetails() {
		return "ID: " + number + "\nName: " + name;
	}
	
	public String moduleDetails() {
		String res = "";
		if(enrolCount == 0) {
			res += "No modules enrolled.";
		}
		
		for(int index = 0; index < enrolCount; index++ ) {
			res += courses[index] +"\n";
		}
		return res;
	}
	
	public String toString() {
		String res = "";
		res += "Student Details\n";
		res += studentDetails() + "\n";
		res += "Modules: \n";
		res += moduleDetails();
		return res;
	}
}
