package Tasks;

public class SumAndAverage { 
	public static void main(String[] args) { 
		int sum = 0;  
		double average; 
		int upperbound = 100; 
		for (int number = 1; number <= upperbound; ++number) {   
			sum += number; 
			}   
		// Compute average in double. Beware that int/int produces int.  
		average = (double)sum / upperbound;
		// Print sum and average. 
		System.out.println("Sum: " + sum);
		System.out.printf("Average: %.2f", average);
		}
	
	}

