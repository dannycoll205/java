package Tasks;
public class DIY02 {

	public static void main(String[] args) {
		printHi();
		printRobot1();
		printRobot2();
		printRobot3();

	}
	public static void printHi() {
		//method to print hi
		System.out.println("*  *  *");
		System.out.println("*  *   ");
		System.out.println("****  *");
		System.out.println("*  *  *");
		System.out.println("*  *  *");
	}
	
	public static void printRobot1() {
		System.out.println("  [@ @]");
		System.out.println(" ~|__|~");
		System.out.println("  d  b");
	}
	
	public static void printRobot2() {
		System.out.println("   .^.");
		System.out.println("  -----");
		System.out.println("  |q p|");
		System.out.println("--[===]--");
		System.out.println("   d b");
	}
	
	public static void printRobot3() {
		System.out.println("  [o o]");
		System.out.println(" ~|# #|~");
		System.out.println("  d  b");
	}
}
