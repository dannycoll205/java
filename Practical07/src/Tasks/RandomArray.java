package Tasks;

import java.util.Random;

public class RandomArray {

	public static void main(String[] args) {
		int random[] = new int[10];
		Random rand = new Random();
		String even = "";
		for(int i = 0; i < random.length; i++) {
			random[i] = rand.nextInt(100);
			
			if(i%2 == 0) {
				System.out.print(random[i] + " ");
			}
			
			if(random[i]%2 == 0) {
				even += random[i] + " ";
			}
		}
		System.out.println();
		System.out.println("Even numbers are " + even);
		
		System.out.println("In reverse order:");
		for(int i = (random.length-1); i >= 0; i--) {
			System.out.print(random[i] + " ");
		}
		
		System.out.println();
		System.out.println("First number is: " + random[0]);
		System.out.println("Last number is: " + random[9]);
		
		

	}

}
