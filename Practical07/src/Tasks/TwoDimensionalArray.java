package Tasks;

public class TwoDimensionalArray {

	public static void main(String[] args) {
		int numbers[][] = new int[5][5];
		int count = 0;
		for(int i = 0; i < 5; i++) {
			for(int j = 0; j < 5; j++) {
				numbers[i][j] = count;
				count++;
			}
		}
		System.out.print("Before\n----------\n");
		for(int i = 0; i < numbers.length; i++) {
			for(int j = 0; j < numbers.length; j++) {
			System.out.print(numbers[i][j] + "\t");
			}
			System.out.println();
		}
		
		for(int i = 0; i < 5; i++) {
			for(int j = i + 1; j < 5; j ++) {
				int swap = numbers[i][j];
				numbers[i][j] = numbers[j][i];
				numbers[j][i] = swap;
			}
		}
		System.out.print("After\n----------\n");
		for(int i = 0; i < numbers.length; i++) {
			for(int j = 0; j < numbers.length; j++) {
			System.out.print(numbers[i][j] + "\t");
			}
			System.out.println();
		}
		
	}
}
