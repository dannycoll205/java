package Tasks;

public class Fibonacci {

	public static void main(String[] args) {  
		// the index n for F(n), starting from n=3 
		int n = 3;  
		// F(n) to be computed   
		int c;  
		// F(n-1) is initialised to F(2)  
		int a = 1;  
		// F(n-2) is initialised to F(1)  
		int b = 1;  
		int nMax = 20;  
		int sum = a + b;   
		double average;  
		System.out.println("The first " + nMax + " Fibonacci numbers are:");  
		System.out.print(a + "\t" + b +"\t");   
		while (n <= nMax) {   
			// Compute F(n), print it and add to sum 
			
			c = a + b;
			a = b;
			b = c;
			
			System.out.print(c + "\t");
			
			sum += c;
			// Adjust the index n and shift the numbers 
			n ++;
			}   
		// Compute and display the average (=sum/nMax)
		average = (double)sum/nMax;
		System.out.println();
		System.out.println(average);
		} 
		
	

}
