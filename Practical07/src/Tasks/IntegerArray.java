package Tasks;

import java.util.Scanner;

public class IntegerArray {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int numbers[] = new int[6];
		int sum, min, max;
		
		for(int i = 0; i < 6; i++) {
			System.out.println("Input a number:");
			numbers[i] = input.nextInt();
		}
		min = numbers[0];
		max = numbers[0];
		sum = 0;
		for(int i = 0; i < 6; i++) {
			sum += numbers[i];
			if(numbers[i] < min) {
				min = numbers[i];
			}
			if(numbers[i] > max) {
				max = numbers[i];
			}
			
		}
		double average = sum/6.0;
		System.out.println("Max: " + max + " Min: " +min + " Average "+ average);
	}

}
