package Tasks;

import java.util.Scanner;

public class DuplicateNumber {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int N = 5;
		String res = "";
		System.out.println("Enter the number of elements: ");
		N = input.nextInt();
		int numbers[] = new int[N];
		
		for(int i = 0; i < N; i++) {
			System.out.println("Enter an integer:");
			numbers[i] = input.nextInt();
		}
		
		for(int i = 0; i < N; i ++) {
			
			for(int j = i+1; j < N; j++) {
				
					if(numbers[i] == numbers[j]) {
						if(!res.contains(numbers[i] + "")) {
							res += numbers[i] + " ";
						}
					
				}
			}
		}

		System.out.println("There is a duplicate of " + res);
	}
		

	
}
