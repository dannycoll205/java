package Tasks;

import java.util.Scanner;

public class Search {

	public static void main(String[] args) {
		String fruit[] = {"apple", "orange", "banana", "kiwi", "strawberry", "tomato", "pineapple",
						"raspberry", "mango", "melon"};
		Scanner input = new Scanner(System.in);
		String choice;
		String res = "";
		System.out.println("What fruit are you looking for?");
		choice = input.next();
		
		for(int i = 0; i < fruit.length; i++) {
			if(choice.equalsIgnoreCase(fruit[i])) {
				res = fruit[i].toUpperCase() + " FOUND!";
				break;
			}else {
				res = choice + " not found.";
			}
		}
		System.out.println(res);
		

	}

}
