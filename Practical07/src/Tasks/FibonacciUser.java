package Tasks;

import java.util.Scanner;

public class FibonacciUser {

	public static void main(String[] args) {  
		Scanner input = new Scanner(System.in);
		// the index n for F(n), starting from n=3 
		int n = 3;  
		// F(n) to be computed   
		int c;  
		// F(n-1) is initialised to F(2)  
		int a = 1;  
		// F(n-2) is initialised to F(1)  
		int b = 1;  
		int nMax = 0;
		boolean invalid = true;
		do{
			System.out.println("Input a number: ");	
			if(!input.hasNextInt()) {
				System.out.println("ERROR");
				input.nextLine();
			}else {
				invalid = false;
				nMax = input.nextInt();
			}
		}
		while(invalid);
		System.out.println("Please Input a positive number:");
		  
		int sum = a + b;   
		double average;  
		System.out.println("The first " + nMax + " Fibonacci numbers are:");  
		System.out.print(a + "\t" + b +"\t");   
		while (n <= nMax) {   
			// Compute F(n), print it and add to sum 
			
			c = a + b;
			a = b;
			b = c;
			
			System.out.print(c + "\t");
			
			sum += c;
			// Adjust the index n and shift the numbers 
			n ++;
			}   
		// Compute and display the average (=sum/nMax)
		average = (double)sum/nMax;
		System.out.println();
		System.out.println(average);
		} 
		
	

}
