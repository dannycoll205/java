package Tasks;

public class ExamResults {

	public static void main(String[] args) {
		String names[] = {"Paul","John", "Brian", "Katie", "Laura", "Eoin",
							"Jamie", "Alex", "Charlie", "Rob"};
		double scores[] = {50,64,46,45,78,98,31,15,45,67};
		double sum = 0;
		double max = 0;
		String res = "";
		String failed = "";
		System.out.println("---Student Report---");
		for(int i = 0; i < names.length; i++) {
			System.out.println("Student Name: " + names[i]);
			System.out.println("Student Mark: " + scores[i]);
			if(scores[i] < 40) {
				System.out.println("Grade: Fail");
				failed += names[i] + "\n";
			}else {
				System.out.println("Grade: Pass");
			}
			System.out.println();
			sum += scores[i];
			
			if(max < scores[i]) {
				res = "The highest mark was "+ scores[i]+ " achieved by "+ names[i];
			}
		}
		double average = sum/scores.length;
		System.out.printf("The class average is: %.2f", average);
		System.out.println();
		System.out.println(res);
		System.out.println();
		System.out.println("***Names of Students that got below 40***");
		System.out.println();
		System.out.print(failed);
	}

	

}
