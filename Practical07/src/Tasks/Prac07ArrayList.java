package Tasks;

import java.util.ArrayList;
import java.util.Scanner; 

public class Prac07ArrayList { 
	public static void main (String[] args) { 
		Scanner sc = new Scanner(System.in); 
		ArrayList<Integer> values = new ArrayList<Integer>();  
		int sum = 0;   int nextValue;  
		System.out.println("Enter a list of integers (press q to quit):"); 
		while (sc.hasNextInt()) {  
			nextValue = sc.nextInt();  
			values.add(nextValue);  
			sum += nextValue;  
		} 
		sc.close();  
		
		System.out.println("The sum of the numbers = " + sum); 
		System.out.println("The numbers in reverse order are:  " );  
		for (int i = values.size()-1; i >= 0; i--) {   
			System.out.print(values.get(i) + " ");  
		}
	}
} 
