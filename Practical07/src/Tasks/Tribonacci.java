package Tasks;

public class Tribonacci {

	public static void main(String[] args) {  
		// the index n for F(n), starting from n=3 
		int n = 4;  
		// F(n) to be computed   
		int d;  
		// F(n-1) is initialised to F(2)  
		int a = 1;  
		// F(n-2) is initialised to F(1)  
		int b = 1;
		int c = 2;
		int nMax = 20;  
		int sum = a + b + c;   
		double average;  
		System.out.println("The first " + nMax + " Fibonacci numbers are:");  
		System.out.print(a + "\t" + b +"\t" + c + "\t");   
		while (n <= nMax) {   
			// Compute F(n), print it and add to sum 
			
			d = a + b + c;
			a = b;
			b = c;
			c = d;
			
			System.out.print(d + "\t");
			
			sum += d;
			// Adjust the index n and shift the numbers 
			n ++;
			}   
		// Compute and display the average (=sum/nMax)
		average = (double)sum/nMax;
		System.out.println();
		System.out.println(average);
		} 
		
	

}
