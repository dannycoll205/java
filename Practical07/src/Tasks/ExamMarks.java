package Tasks;

public class ExamMarks {

	public static void main(String[] args) {
		double john[] = {1,2,3,4,5,6,7,8,9,0,10,20,30,40,50,60,70,80,90,0};
		int count = 0;
		for(int i = 0; i < john.length; i++) {
			if(john[i] < 40) {
				count++;
			}
		}
		System.out.println(john.length + " students took the exam and " + count + " failed.");
	}

}
