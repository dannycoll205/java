package Tasks;

public class Student {
	private String name;
	private int studentNum;
	private double[] marks = new double[6];
	private int amount = 0;
	
	public Student(String name, int num) {
		this.name = name;
		this.studentNum = num;
	}
	
	public void enrolOnModule(String module) {
		System.out.println(name + " enrols on "+module);
	}
	
	public void takeExam(String exam) {
		System.out.println(name +" takes "+exam);
	}
	
	public void addMark(double m) {
		marks[amount] = m;
		amount++;
	}
	
	public void displayStudent() {
		System.out.println(studentNum +"\t"+name);
		System.out.print("Marks: ");
		for(int i=0; i<marks.length;i++) {
			System.out.printf("%.2f",marks[i]);
			if(i!=(marks.length-1)) {
				System.out.print(", ");
			}
		}
		System.out.println();
	}
	
	public void generateRandomNumbers() {
		for(int i=0; i<marks.length;i++) {
			double mark = Math.random()*100;
			marks[i]=mark;
		}
	}
	
	public double bestMark() {
		double max = 0;
		for(double mark:marks) {
			if(mark>max) {
				max = mark;
			}
		}
		return max;
	}
	
	public double worstMark() {
		double min = 100;
		for(double mark:marks) {
			if(mark<min) {
				min = mark;
			}
		}
		return min;
	}
	
	public double averageMark() {
		double total = 0;
		for (double mark:marks) {
			total+=mark;
		}
		double avg = total/marks.length;
		return avg;
	}
	//getters
	public String getName() {
		return name;
	}

	public int getStudentNum() {
		return studentNum;
	}
	
	public double[] getMarks() {
		return marks;
	}
	
}
