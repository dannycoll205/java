package Tasks;

import java.util.ArrayList;

public class StudentTester {

	public static void main(String[] args) {
		ArrayList<Student> Students = new ArrayList<Student>();
		
		Student s1 = new Student("Joe Bloggs", 3456123);
		s1.enrolOnModule("Maths");
		s1.takeExam("Maths");
		s1.addMark(24.5);
		s1.addMark(55.75);
		s1.addMark(80.5);
		s1.addMark(44.5);
		s1.addMark(50);
		s1.addMark(37);
		s1.displayStudent();
		
		Student s2 = new Student("Mary Jones", 77774321);
		s2.generateRandomNumbers();
		s2.displayStudent();
		System.out.printf("%.2f",s2.worstMark());
		//adding Students
		Students.add(s1);
		Students.add(s2);
		Students.add(new Student("Dave",123));
		Students.add(new Student("Mike", 456));
		Students.add(new Student("Molly", 789));
		Students.add(new Student("Amy", 987));
		Students.add(new Student("Wayne", 654));
		Students.add(new Student("Rob", 321));
		//giving marks for each Student
		for(Student eachS:Students) {
			eachS.generateRandomNumbers();
		}
		System.out.println();
		System.out.println("===========Current Students==========");
		for(Student eachS:Students) {
			eachS.displayStudent();
		}
		System.out.println(averageAll(Students));
	}
	
	public static String highestAverage(ArrayList<Student> all) {
		double highestAvg = 0;
		Student bestStudent = null;
		for(Student eachS:all) {
			
			double avg = eachS.averageMark();
			if(avg>highestAvg) {
				highestAvg = avg;
				bestStudent = eachS;
			}
		}
		return bestStudent.getName();
	}
	
	public static String lowestAverage(ArrayList<Student> all) {
		double lowestAvg = 100;
		Student worstStudent = null;
		for(Student eachS:all) {
			double avg = eachS.averageMark();
			if(avg<lowestAvg) {
				lowestAvg = avg;
				worstStudent = eachS;
			}
		}
		return worstStudent.getName();
	}
	
	public static double averageAll(ArrayList<Student> all) {
		double total = 0;
		for(Student eachS:all) {
			total+=eachS.averageMark();
			}
			
		double result = total/all.size();
		return result;
	}
}
