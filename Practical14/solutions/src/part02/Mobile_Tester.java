package part02;

import java.util.ArrayList;

public class Mobile_Tester {

	public static void main(String[] args) {
		//part (b)
		//Create a polymorphic arrayList with a pay as you go and a monthly contract
		ArrayList<Contract> my_contracts = new ArrayList<Contract>();
		my_contracts.add(new Monthly(250));
		my_contracts.add(new PayAsYouGo(10));
		/*
		 * Alternatively you could have carried out part (b) in multiple steps:
		 * Monthly myM = new Monthly(250);
		 * PayAsYouGo myP = new PayAsYouGo(10);
		 * my_contracts.add(myM);
		 * my_contracts.add(myP);
		 */
		//Test the exception handling (e)
		try {      
			((Monthly) my_contracts.get(0)).sendTexts(101);
			((PayAsYouGo) my_contracts.get(1)).sendTexts(101);

		}
		catch(InsufficientFundsException ife) {
			System.out.println(ife.getMessage());
		}
	}

}
