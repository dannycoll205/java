package part01;

public class Employee extends Person implements Duties {
	public static int nextID = 0;
	private int id;
	private Department dept;
	private String dailyTask;
	
	public Employee(String forename,String surname, Department dept, String dailyTask) {
		super(forename, surname);
		this.dept=dept;
		this.dailyTask=dailyTask;
		nextID++;
		this.id = nextID;		
	}
	@Override
	public void conductTask() {
		System.out.println("Conducting task: "+this.dailyTask);
	}
	@Override
	public String getDetails() {
		return super.getDetails()+" (ID:"+this.id+")";
	}
	public int getID() {
		return this.id;
	}
	public Department getDept() {
		return this.dept;
	}
}
