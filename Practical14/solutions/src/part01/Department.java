package part01;

public enum Department {
	SPA_WELLBEING{ 
		@Override
		public String description() {
		return "Spa and Wellbeing";
	}
	},
	CATERING{ 
		@Override
		public String description() {
		return "Catering";
	}
	},
	HOUSEKEEPING{
		@Override
		public String description() {
		return "Hoursekeeping";
	}
	},
	HOSPITALITY{
		@Override
		public String description() {
		return "Hospitality";
	}
	};
	//Declare the abstract method description (italics on the UML)
	//Override the method immediately after each element of the 
	//enumeration class
	public abstract String description();
}
