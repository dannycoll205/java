package part01;

import java.util.ArrayList;

public class Employee_tester {

	public static void main(String[] args) {
		//Define five instances of the Employee object (e)
		Employee amy_smyth = new Employee("Amy","Smyth",Department.SPA_WELLBEING,"massage");
		Employee pat_stewart = new Employee("Patrick","Stewart",Department.SPA_WELLBEING,"pool cleaning");
		Employee john_fraser = new Employee("John","Fraser",Department.CATERING,"cooking");
		Employee sheila_williams = new Employee("Sheila","Williams",Department.HOSPITALITY,"check in");
		Employee jim_hopkins = new Employee("Jim","Hopkins",Department.HOUSEKEEPING,"cleaning");

		//Create an arraylist and store all the employees (e)
		ArrayList<Employee> all_employees = new ArrayList<Employee>();
		all_employees.add(amy_smyth);
		all_employees.add(pat_stewart);
		all_employees.add(john_fraser);
		all_employees.add(sheila_williams);
		all_employees.add(jim_hopkins);
		
		//Method to print out all staff information (f) -see below
		printEmployees(all_employees);
		
		//All staff in the SPA_WELLBEING department
		int team_size = teamSize(all_employees,Department.SPA_WELLBEING);
		System.out.println("The number of employees in the Spa and Wellbeing team is "+team_size);
		//Alternatively you could have been more general and said:
		//System.out.println("The number of employees in the "+Department.SPA_WELLBEING+"is "+team_size);
		
		//Overloading teamSize to show how many staff there are in total
		int total_team_size = teamSize(all_employees);
		System.out.println("The number of employees in the hotel is "+total_team_size);
	}
	//This method prints the Employee details as per (f)
	public static void printEmployees(ArrayList<Employee> all_employees) {
		for(Employee e:all_employees) {
			System.out.println("Staff member: "+e.getDetails());
			System.out.println("Department: "+e.getDept().description());
			e.conductTask();
			System.out.println();
		}
	}
	//part (g)
	public static int teamSize(ArrayList<Employee> all_employees,Department d){
		int counter = 0;
		for(Employee e:all_employees) {
			if(e.getDept().equals(d))
				counter++;
		}
		return counter;
	}
	
	//part (h)
	public static int teamSize(ArrayList<Employee> all_employees){
		return all_employees.size();
	}

}
