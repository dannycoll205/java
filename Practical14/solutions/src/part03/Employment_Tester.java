package part03;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
public class Employment_Tester {

	public static void main(String[] args) {
		File myFile = new File("C:/Users/3053508/Desktop/CSC1020_code/Mock_assessment_w7/src/part03/employment-nics.csv");
		Scanner myScanner;
		
		double total_male = 0;
		double total_female = 0;
		try {
			myScanner = new Scanner(myFile);
			//remove header line
			myScanner.nextLine();
			while(myScanner.hasNextLine())
			{
				String currentLine = myScanner.nextLine();
				String[] lineParts = currentLine.split(",");
				total_male+=Integer.parseInt(lineParts[1].trim());
				total_female+=Integer.parseInt(lineParts[2].trim());
			}
			myScanner.close();
			System.out.printf("The percentage of males employed is %.1f",(total_male/(total_male+total_female))*100);
			System.out.printf("\nThe percentage of females employed is %.1f",(total_female/(total_male+total_female))*100);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
