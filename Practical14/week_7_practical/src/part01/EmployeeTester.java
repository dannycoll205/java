package part01;

import java.util.ArrayList;

public class EmployeeTester {
	public static void main(String[] args) {
		Employee e1 = new Employee("Amy","Smyth",Department.SPA_WELLBEING,"massage");
		Employee e2 = new Employee("Patrick","Stewart",Department.SPA_WELLBEING,"pool cleaning");
		Employee e3 = new Employee("John","Fraser",Department.CATERING,"cooking");
		Employee e4 = new Employee("Sheila","Williams",Department.HOSPITALITY,"check in");
		Employee e5 = new Employee("Jim","Hopkins",Department.HOUSEKEEPING,"cleaning");
		
		ArrayList<Employee> myEmployees =new ArrayList<Employee>();
		
		myEmployees.add(e1);
		myEmployees.add(e2);
		myEmployees.add(e3);
		myEmployees.add(e4);
		myEmployees.add(e5);
		
		printEmployees(myEmployees);
		System.out.println("The number of employees in the hotel is "+teamSize(myEmployees));
		System.out.println("The number of employees in the Spa and Wellbeing team is "+teamSize(myEmployees, Department.SPA_WELLBEING));
	}
	public static void printEmployees(ArrayList<Employee> all) {
		for(Employee each:all) {
			System.out.println(each.getDetails());
			System.out.println("Department: "+each.getDept());
			each.conductTask();
		}
	}
	public static int teamSize(ArrayList<Employee> all, Department dept) {
		int amount=0;
		for(Employee each:all) {
			if(each.getDept()==dept) {
				amount++;
			}
		}
		return amount;
	}
	public static int teamSize(ArrayList<Employee> all) {
		return all.size();
		
	}

}
