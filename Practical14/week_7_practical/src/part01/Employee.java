package part01;


public class Employee extends Person implements Duties{
	
	private int id;
	private static int nextId;
	private Department dept;
	private String dailyTask;
	
	public Employee(String forename, String surname, Department dept, String dailyTask) {
		super(forename,surname);
		this.dailyTask=dailyTask;
		this.dept=dept;
		nextId ++;
		this.id = nextId;
		
	}
	@Override
	public String getDetails() {
		return "Name"+getForename()+" "+getSurname()+" (ID:"+this.id+")";
	}
	public int getId() {
		// TODO Auto-generated method stub
		return id;
	}
	
	public Department getDept() {
		return dept;
	}
	@Override
	public void conductTask() {
		System.out.println("Conducting task: "+this.dailyTask);
	}
	
	
	
}
