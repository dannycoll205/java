package part02;
//part (c)
public class InsufficientFundsException extends Exception {
	// note that 'message' is a String passed in as a parameter 
	//to the constructor of this class.
	public InsufficientFundsException(String message) {
		super("You don't have sufficient credit to send these messages, you need to " + message);
	}
}
