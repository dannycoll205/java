package Tasks;

public class PassFail2 {

	public static void main(String[] args) {
		double examMark = 68;
		
		System.out.println("The mark is " + examMark); 
		if (examMark >= 70) { 
			System.out.println("Distinction"); 
		} else if (examMark >= 60){ 
			System.out.println("Commendation"); 
		} else if(examMark >= 50) {
			System.out.println("Pass");
		} else {
			System.out.println("Fail");
		}

	}

}
