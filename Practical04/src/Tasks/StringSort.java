package Tasks;
import java.util.Scanner;

public class StringSort {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		//Set up data required
		String word1, word2, word3, first, second, third;
		
		//Ask for and read 3 words from user
		System.out.print("Enter word 1: ");
		word1 = input.next();
		
		System.out.print("Enter word 2: ");
		word2 = input.next();
		
		System.out.print("Enter word 3: ");
		word3 = input.next();
		
		//Sort into alphabetical order
		if (word1.compareTo(word2) < 0 && word1.compareTo(word3) < 0) { //word1 is less than word2 so comes before
			first = word1;
			if(word2.compareTo(word3) < 0) {
				second = word2;
				third = word3;
			}else {
				second = word3;
				third = word2;
			}
		} else if (word2.compareTo(word1) < 0 && word2.compareTo(word3) < 0) {
			first = word2;
			if(word1.compareTo(word3) < 0) {
				second = word1;
				third = word3;
			}else {
				second = word3;
				third = word1;
			}
		} else {
			first = word3;
			if(word2.compareTo(word3) < 0) {
				second = word2;
				third = word1;
			}else {
				second = word1;
				third = word2;
			}
		}
		
		//Print words in original and new order
		System.out.println("Original words: ");
		System.out.println(word1 + ", " + word2 + ", " + word3 + ".");
		
		System.out.println("Sorted words: ");
		System.out.println(first + ", " + second + ", " + third + ".");
		
		
		input.close();
	}

}
