package Tasks;

public class CircleComputation {

	public static void main(String[] args) {
		double radius = 5;
		double area = Math.PI * Math.pow(radius, 2);
		double circumf = 2 * Math.PI * radius;
		
		System.out.println("The radius is " + radius);
		System.out.printf("The area is %.2f\n", area);
		System.out.printf("The circumfrence is %.2f\n", circumf);

	}

}
