package Tasks;

import java.util.Scanner;

public class BillCalculator {
	public static double changeDue = 0;
	public static int changeRemaining = 0;
	
	public static void main(String[] args) {
		final int PENCE_IN_TWENTY_POUND = 2000;
		final int PENCE_IN_TEN_POUND = 1000;
		final int PENCE_IN_FIVE_POUND = 500;
		final int PENCE_IN_POUND = 100;
		final int PENCE_IN_FIFTY = 50;
		final int PENCE_IN_TWENTY = 20;
		final int PENCE_IN_TEN = 10;
		final int PENCE_IN_FIVE = 5;
		final int PENCE_IN_TWO = 2;
		
		double first, second, third, fourth, fifth, totalPrice, cashAmount;
		Scanner input = new Scanner(System.in);
		
		System.out.println("Enter the prices one by one: ");
		
			first = input.nextDouble();
			second = input.nextDouble();
			third = input.nextDouble();
			fourth = input.nextDouble();
			fifth = input.nextDouble();
			
			
		totalPrice = first + second + third + fourth + fifth;
		
		System.out.printf("The cost is: " + totalPrice + "p.");
		System.out.println("How much money did the customer give?");
		cashAmount = input.nextInt();
		input.close();
		
		changeDue = (cashAmount - totalPrice) * 100;
		
		changeRemaining = (int) changeDue;
		
		int twentyNotes = calcNumCoins(PENCE_IN_TWENTY_POUND);
		int tenNotes = calcNumCoins(PENCE_IN_TEN_POUND);
		int fiveNotes = calcNumCoins(PENCE_IN_FIVE_POUND);
		int poundCoins = calcNumCoins(PENCE_IN_POUND);
		int fiftyCoins = calcNumCoins(PENCE_IN_FIFTY);
		int twentyCoins = calcNumCoins(PENCE_IN_TWENTY);
		int tenCoins = calcNumCoins(PENCE_IN_TEN);
		int fiveCoins = calcNumCoins(PENCE_IN_FIVE);
		int twoCoins = calcNumCoins(PENCE_IN_TWO);
		int oneCoins = changeRemaining;
		
		System.out.println(twentyNotes + " Twenty Pounds");
		System.out.println(tenNotes + " Ten Pounds");
		System.out.println(fiveNotes + " Five Pounds");
		System.out.println(poundCoins + " Pound Coins");
		System.out.println(fiftyCoins + " 50p");
		System.out.println(twentyCoins + " 20p");
		System.out.println(tenCoins + " 10p");
		System.out.println(fiveCoins + " 5p");
		System.out.println(twoCoins + " 2p");
		System.out.println(oneCoins + " 1p");
		
		
	}
	public static int calcNumCoins(int divisor) {
		int numCoins = changeRemaining / divisor;
		changeRemaining = changeRemaining % divisor;
		return numCoins;
	}
}
