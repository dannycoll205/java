package Tasks;

import java.util.Scanner;
public class NewCalculator {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		 System.out.println("Pick 2 numbers. First: ");
		 int a = input.nextInt();
		 System.out.println("Second: ");
		 int b = input.nextInt();
		 
		 int sum, difference, product, max, min;
		 double average;
		 
		 sum = a + b;
		 difference = a - b;
		 product = a * b;
		 average = (double) (a + b) / 2;
		 max = Math.max(a,b);
		 min = Math.min(a, b);
		 
		 
		 System.out.println("Sum: \t\t" + sum);
		 System.out.println("Difference: \t" + difference);
		 System.out.println("Product: \t" + product);
		 System.out.println("Average: \t" + average);
		 System.out.println("Maximum: \t" + max);
		 System.out.println("Minimum: \t" + min);
		 
		 
		 input.close();
	}

}
