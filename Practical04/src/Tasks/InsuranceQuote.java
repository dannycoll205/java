package Tasks;

import java.util.Scanner;

public class InsuranceQuote {

	public static void main(String[] args) {
		
		double cost = 200;
		int age =0;
		int noClaims;
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Please input your insurance type: 1) Comp. \n\t2) Fire and Theft \n\t3) Third Party");
		int insurance = input.nextInt();
		
		
		
		switch(insurance) {
		case 1: cost += 200;
			break;
		case 2: cost += 100;
			break;
		case 3: cost += 0;
			break;
			default: System.out.println("Invalid Choice");
					cost = 0;
		} //end of switch
		
		System.out.println("Please provide your age: ");
		if(input.hasNextInt()) {
			age = input.nextInt();
			if(age < 17) {
				System.out.println("Sorry, you are too young to get a quote.");
				cost = 0;
			} else if(age <= 20){
				cost += 900;
			} else if(age <= 30) {
				cost += 300;
			} else if(age <= 40) {
				cost += 100;
			}
		}else {
			System.out.println("Invalid Input, must be an integer value.");
		}			//end of if/else for checking input was a number
		
		System.out.println("Please provide how many years No Claims you have: ");
		if(input.hasNextInt()) {
			noClaims = input.nextInt();
			if(age - noClaims < 17) {
				System.out.println("Invalid Input, you would have been too young to drive " + noClaims + " years ago.");
				cost = 0;
			}else {			
			
				switch(noClaims) {
				case 1: cost = cost * 0.9;
					break;
				case 2: cost = cost * 0.8;
					break;
				case 3: cost = cost * 0.7;
					break;
				case 4: cost = cost * 0.6;
					break;
				default: cost = cost * 0.5;
				}//end of switch
			}
		}else {
			System.out.println("Invalid Input, must be an integer value.");
		}//end of if/else for checking input was a number
		
		input.close(); //close scanner
		System.out.print("Your insurance quote is: ");
		System.out.printf("�%.2f", cost);
	}

}
