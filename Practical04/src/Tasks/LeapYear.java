package Tasks;

import java.util.Scanner;

public class LeapYear {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int year;
		
		System.out.println("Input a year to check: ");
		//ensure input is an integer	
		if(input.hasNextInt()) {
			year = input.nextInt();
			//must be greater than 0
			if(year <= 0) {
				System.out.println("Invalid Input");
				
			} //ensure it is divisible by 4
			else if (year % 4 == 0) {
				//make sure it is not divisible by 100
				if(year % 100 == 0 && year % 400 != 0 ) {
					System.out.println("The year " + year + " is not Leap Year.");
				} else {
					System.out.println("The year " + year + " is a Leap Year.");
				}
			} else {
				System.out.println("The year " + year + " is not a Leap Year");
			}
		}else {
			String text = input.next();
			System.out.println("Invalid Input. Need to enter a number not: " + text);
		}
		input.close();
	}

}
