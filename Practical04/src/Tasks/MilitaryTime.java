package Tasks;
import java.util.Scanner;

public class MilitaryTime {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int firstTime = 0;
		int secondTime = 0;
		int hours = 0;
		int minutes = 0;
		System.out.println("Enter your first time ");
		//firstTime = input.nextInt();
		if(input.hasNextInt()) {
			firstTime = input.nextInt();
			if(firstTime < 0 || firstTime > 2400) {
				System.out.println("Error: Incorrect Input");
			}
				
		}else {
			System.out.println("Error: Incorrect Input");
		}
		
		System.out.println("Enter your second time ");
		if(input.hasNextInt()) {
			secondTime = input.nextInt();
			if(secondTime < 0 || secondTime > 2400) {
				System.out.println("Error: Incorrect Input");
			}
				
		}else {
			System.out.println("Error: Incorrect Input");
		}
			
		
		hours = Math.abs(firstTime / 100) - (secondTime / 100);
		minutes = (firstTime % 100) - (secondTime % 100);
	
		if(minutes < 0) {
			hours --;
			minutes = Math.abs(minutes);
		}		
			
		System.out.println("The difference is: " + hours + " hours and " + minutes + " minutes.");
		input.close();
	}

}
