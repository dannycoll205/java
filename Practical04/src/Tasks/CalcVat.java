package Tasks;
import java.util.Scanner;

public class CalcVat {
	static final double VATRATE = 20;
	static double price; 
	static double vat; 
	public static void main (String[] args) { 
		// declare the VAT rate as a final 
		 
			 
		Scanner input = new Scanner(System.in);
		// declare the purchase price 
		 
		System.out.println("Please enter the cost of the purchase: ");
		price = input.nextDouble();
		
		// declare a variable to hold the VAT to be paid  
		 
		 
		// Do the calculation 
		vat = VATRATE / 100 * price;
		
			 
		// output the answer
		System.out.printf("VAT to pay is: �%.2f\n", vat);
		input.close();
	}
}
