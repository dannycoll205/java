package Tasks;

public class PassFail {
	public static void main(String[] args) { 
		int examMark = 49; 
		
		System.out.println("The mark is " + examMark); 
		if (examMark >= 50) { 
			System.out.println("Pass"); 
		} else { 
			System.out.println("Fail"); 
		}
	}

}
