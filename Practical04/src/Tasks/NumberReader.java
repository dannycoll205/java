package Tasks;
import java.util.Scanner;

public class NumberReader {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int number;
		
		System.out.println("Input a number between 1,000 and 999,999: ");
		if (input.hasNextInt()) {
			number = input.nextInt();
			if(number < 1000 || number > 999999) {
				System.out.println("Error: Value must be between 1,000 and 999,999.");
			} else {
				String value = String.valueOf(number);
				
				String part1 = value.substring(0, value.length() - 3);
				String part2 = value.substring(value.length() - 3);
				System.out.println(part1 + "," + part2);
			}
		} else {
			String text = input.next();
			System.out.println("Error: Need to input a number not: " + text);
		}
		input.close();
	}

}
