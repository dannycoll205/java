package Tasks;
import java.util.Scanner;

public class NameGenerator {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		String name, pet, city, favCity, newName, newPlace;
		
		System.out.println("Give your name, first pet, home town and favourite city: ");
		name = input.next();
		pet = input.next();
		city = input.next();
		favCity = input.next();
		
		//System.out.println(name + pet + city + favCity);
		name = name.toUpperCase();
		pet = pet.toUpperCase();
		city = city.toUpperCase();
		favCity = favCity.toUpperCase();
		
		newName = name.substring(0, name.length()/2 + 1) + city.substring(city.length()/2 + 1);
		newPlace = pet.substring(0, 1) + favCity.substring(favCity.length()/2 + 1) + city.substring(0, city.length()/2);
		
		System.out.println(newName);
		System.out.println(newPlace);
		
		input.close();
	}

}
