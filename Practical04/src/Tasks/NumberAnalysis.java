package Tasks;

import java.util.Scanner;

public class NumberAnalysis {

	public static void main(String[] args) {
		//Set up data needed
		int a, b, c, d, e;
		int sum = 0;
		int max, min;
		double average;
		Scanner input = new Scanner(System.in);
		
		//Ask for and read in values
		System.out.println("Enter a number: ");
		a = input.nextInt();
		
		System.out.println("Enter a number: ");
		b = input.nextInt();
		
		System.out.println("Enter a number: ");
		c = input.nextInt();
		
		System.out.println("Enter a number: ");
		d = input.nextInt();
		
		System.out.println("Enter a number: ");
		e = input.nextInt();
		
		//Compute values
		sum = a + b + c + d + e;
		average = sum / 5.0;
		
		if ( a < b) {
			min = a;
			max = b;
		}else {
			min = a;
			max = b;
		}
		
		//Output values
		System.out.println(average);
		input.close();
	}

}
