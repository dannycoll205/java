package Tasks;

import java.util.Scanner;

public class CinemaCost {

	public static void main(String[] args) {
		double ticketCost;
		int age, time;
		String ans;
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("What age are you?");
		if(input.hasNextInt()) {
			age = input.nextInt();
			if(age < 0) {
				System.out.println("Invalid input, age must be positive.");
				ticketCost = 0;
			} else if(age <= 18) {
				ticketCost = 4.00;
			} else {
				ticketCost = 8.00;
			}
		} else {
			System.out.println("Invalid input, age must be a number.");
			ticketCost = 0;
		}
		System.out.println("What time is it? (in 24hr format)");
		if(input.hasNextInt()) {
			time = input.nextInt();
			if(time < 0 || time > 2400) {
				System.out.println("Invalid input, time must be between 0 and 24 hours");
				}
			else if(time < 1700) {
				ticketCost = 2.00;
				System.out.printf("Your ticket price is: �%.2f", ticketCost);
			}
			else {
				System.out.println("Are you a student? (y/n)");
				ans = input.next();
				if(ans.compareTo("y") == 0) {
					ticketCost = 6.00;
					System.out.printf("Your ticket price is: �%.2f", ticketCost);
				}else if(ans.compareTo("n") == 0) {
					ticketCost = ticketCost;
					System.out.printf("Your ticket price is: �%.2f", ticketCost);
				}else {
					System.out.println("Error: Invalid Input.0");
				}
		input.close();
		
				}
			
		}
}
	}
