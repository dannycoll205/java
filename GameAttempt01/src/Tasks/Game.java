package Tasks;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;

public class Game extends Canvas implements Runnable{
	private Thread thread;
	private boolean isRunning = false;
	private KeyInput input;
	private MouseInput mInput;
	private Camera camera;
	
	private Handler handler;
	public static void main(String[] args) {
		new Game();
	
	}

	private void init() {
		handler = new Handler();

		//camera = new Camera(0,0,handler);
		
		input = new KeyInput();
		mInput = new MouseInput(handler,camera);
		this.addMouseListener(mInput);
		this.addKeyListener(input);
		
		handler.addObject(new Player(100,100,ID.Player,input));
		mInput.findPlayer();
		handler.addObject(new Box(100,100,ID.Block));
		handler.addObject(new Box(50,100,ID.Block));
		handler.addObject(new Box(150,100,ID.Block));
	}
	//initial values for window	
	public static int WIDTH=800, HEIGHT=608;
	public static String title = "Game Hopefully?";
	public Game() {
		new Window(WIDTH,HEIGHT,title,this);
		start();
		
		init();
		Bullet b1 = new Bullet(100,100,ID.Bullet);
		handler.addObject(b1);
		
	}
	
	private synchronized void start() {
		if(isRunning) return;
		thread = new Thread(this);
		thread.start();
		isRunning=true;
	}
	
	private synchronized void stop() {
		if(!isRunning) return;
		
		try {
			thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		isRunning=false;
	};
	//gameloop
	public void run() {
		this.requestFocus();
		long lastTime = System.nanoTime();
		double noOfTicks = 60.0;
		double ns = 1000000000/noOfTicks;
		double delta = 0;
		
		long timer = System.currentTimeMillis();
		int frames = 0;
		while(isRunning) {
			long now = System.nanoTime();
			delta+=(now-lastTime)/ns;
			lastTime = now;
			while(delta>=1) {
				tick();
				delta--;
			}
			render();
			frames++;
			
			if(System.currentTimeMillis()-timer>1000) {
				timer+=1000;
				frames=0;
			}
		}
		stop();
	}

	private void tick() {
		handler.tick();
		//camera.tick();
	}
	
	private void render() {
		BufferStrategy bs = this.getBufferStrategy();
		if(bs==null) {
			this.createBufferStrategy(3);
			return;
		}
		
		Graphics g = bs.getDrawGraphics();
		Graphics2D g2d = (Graphics2D) g;
		
		g.setColor(Color.cyan);
		g.fillRect(0,0,WIDTH, HEIGHT);
		
		//g2d.translate(-camera.getX(), -camera.getY());
		handler.render(g);
		//g2d.translate(camera.getX(), camera.getY());
		
		bs.show();
		g.dispose();
	};

}
