package Tasks;

import java.awt.Color;
import java.awt.Graphics;

public class Bullet extends GameObject{

	public Bullet(float x, float y, ID id) {
		super(x, y, id);
		velX = 10;
		velY = 10;
	}

	@Override
	public void tick() {
		// TODO Auto-generated method stub
		x+=velX;
		y+=velY;
		
	}

	@Override
	public void render(Graphics g) {
		// TODO Auto-generated method stub
		g.setColor(Color.red);
		g.fillRect((int)x,(int)y, 8, 8);
	}

}
