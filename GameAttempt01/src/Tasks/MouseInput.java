package Tasks;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MouseInput extends MouseAdapter{
	
	private Handler handler;
	private Camera camera;
	private GameObject tempPlayer =null;
	
	public MouseInput(Handler handler,Camera camera) {
		this.handler=handler;
		this.camera=camera;
	}
	
	public void findPlayer() {
		for(int i=0;i<handler.object.size();i++) {
			if(handler.object.get(i).getId()==ID.Player) {
				tempPlayer = handler.object.get(i);
				break;
			}
		}
	}
	
	public void mousePressed(MouseEvent e) {
		int mX=e.getX();
		int mY=e.getY();
		if(tempPlayer != null) {
			GameObject tempBullet = handler.addObject(new Bullet(tempPlayer.x+16,tempPlayer.y+16,ID.Bullet));
			
			float angle = (float) Math.atan2(mY-tempPlayer.y-16+camera.getY(),mX-tempPlayer.x-16+camera.getX());
			int bulletVel=10;
			
			tempBullet.velX = (float) (bulletVel*Math.cos(angle));
			tempBullet.velY = (float) (bulletVel*Math.sin(angle));
			
			
		}else findPlayer();
	}


}
