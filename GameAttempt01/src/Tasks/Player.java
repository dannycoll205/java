package Tasks;

import java.awt.Color;
import java.awt.Graphics;

public class Player extends GameObject {
	private float acc = 1f;
	private float dcc = 0.5f;
	private KeyInput input;
	
	public Player(float x, float y, ID id, KeyInput input) {
		super(x, y, id);
		// TODO Auto-generated constructor stub
		
		this.input = input;
	}

	public void tick() {
		// TODO Auto-generated method stub
		x+=velX;
		y+=velY;
		
		//horizontal
		if(input.keys[0]) velX+=acc;
		else if(input.keys[1]) velX-=acc;
		else if(!input.keys[0]&&!input.keys[1]) {
			if(velX>0) velX-=dcc;
			else if(velX<0) velX+=dcc;
		}
		//vertical
		if(input.keys[2]) velY-=acc;
		else if(input.keys[3]) velY+=acc;
		else if(!input.keys[2]&&!input.keys[3]) {
			if(velY>0) velY-=dcc;
			else if(velY<0) velY+=dcc;
		}
		
		velX = clamp(velX,5,-5);
		velY = clamp(velY,5,-5);
		
	}
	
	private float clamp(float value, float max, float min) {
		if(value>=max) value=max;
		else if(value<=min) value=min;
		
		return value;
	}

	@Override
	public void render(Graphics g) {
		// TODO Auto-generated method stub
		g.setColor(Color.black);
		g.fillRect((int)x,(int)y, 32, 32);
		
	}

	

}
