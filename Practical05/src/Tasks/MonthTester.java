package Tasks;

import java.util.Scanner;

public class MonthTester {

	public static void main(String[] args) {
		int monthNumber;
		Scanner input = new Scanner(System.in);
		
		System.out.println("Enter a month betweek 1 and 12: ");
		monthNumber = input.nextInt();
		
		Month usersMonth = new Month(monthNumber);
		
		System.out.print(usersMonth.getDays(monthNumber) + " days.");
		
	}

}
