package Tasks;

import java.util.Scanner;

public class NumberSorter {

	public static void main(String[] args) {
		int num1, num2, num3, first, second, third;
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Input your first number: ");
		if(input.hasNextInt()) {
			num1 = input.nextInt();
			System.out.println("Input your second number: ");
			if(input.hasNextInt()) {
				num2 = input.nextInt();
				System.out.println("Input your third number: ");
				if(input.hasNextInt()) {
					num3 = input.nextInt();
					if(num1 > num2 && num2 > num3) {
						System.out.println("The numbers " + num1 + ", " + num2 + " and " + num3 + " are decreasing.");

					}else if (num1 < num2 && num2 < num3) {
						System.out.println("The numbers " + num1 + ", " + num2 + " and " + num3 + " are increasing.");

					}else {
						System.out.println("The numbers " + num1 + ", " + num2 + " and " + num3 + " are neither increasing or decreasing.");
						if(num1 < num2) {
							first = num1;
							second = num2;
							if(num3 < num1) {
								first = num3;
								second = num1;
								third = num2;
							}else {
								third = num3;
							}
						}else { //num2 < num1
							first = num2;
							second = num1;
							if(num3 > num2) {
								second = num3;
								third = num1;
							}else {
								third = num3;
							}
						}
						System.out.println("Your numbers in ascending order are: " + first + ", " + second + ", " + third);

					}
				}else {
					System.out.println("ERROR: Input must be an integer.");
				}
			}else {
				System.out.println("ERROR: Input must be an integer.");
			}
		}else {
			System.out.println("ERROR: Input must be an integer.");
		}
	}

}
