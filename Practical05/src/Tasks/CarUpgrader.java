package Tasks;

import java.util.Scanner;

public class CarUpgrader {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String details;
		int basicCost = 11000;
		int cost;
		
		cost = basicCost;
		Scanner input = new Scanner(System.in);
		System.out.println("BASIC CAR MODEL COSTS: �11,000.");
		System.out.println("It includes:");
		System.out.println("\t- 1.21 petrol engine");
		System.out.println("\t- Silver colour\n");
		
		System.out.println("Would you like to upgrade the engine?");
		System.out.println("y for yes");
		System.out.println("n for no");
		
		details = input.next();
		while(!details.equals("y") && !details.equals("n")) {
			System.out.println("ERROR: Invalid input.");
			System.out.println("Would you like to upgrade the engine?");
			System.out.println("y for yes");
			System.out.println("n for no");
			
			details = input.next();
		}
		
		if(details.equals("y")) {
			System.out.println("What engine would you like to upgrade to?");
			System.out.println("a. 1.4l petrol (add �200)");
			System.out.println("b. 1.6l petrol (add �300)");
			System.out.println("c. 1.8l petrol (add �400");
			details = input.next();
			
			while(!details.equals("a") && !details.equals("b") && !details.equals("c")) {
				System.out.println("ERROR: Invalid Input.");
				System.out.println("What engine would you like to upgrade to?");
				System.out.println("a. 1.4l petrol (add �200)");
				System.out.println("b. 1.6l petrol (add �300)");
				System.out.println("c. 1.8l petrol (add �400");
				details = input.next();
			}
			
			if(details.equals("a")) {
				cost += 200;
			}else if(details.equals("b")) {
				cost += 300;
			}else {
				cost += 400;
			}
		}
		
		System.out.println("Would you like to change the car colour?");
		System.out.println("y for yes");
		System.out.println("n for no");
		
		details = input.next();
		while(!details.equals("y") && !details.equals("n")) {
			System.out.println("ERROR: Invalid input.");
			System.out.println("Would you like to change the car colour?");
			System.out.println("y for yes");
			System.out.println("n for no");
			
			details = input.next();
		}
		
		if(details.equals("y")) {
			System.out.println("What colour would you like to change to?");
			System.out.println("a. Red (add �200)");
			System.out.println("b. Green (add �300)");
			System.out.println("c. White (add �400)");
			details = input.next();
			
			while(!details.equals("a") && !details.equals("b") && !details.equals("c")) {
				System.out.println("ERROR: Invalid Input.");
				System.out.println("What colour would you like to change to?");
				System.out.println("a. Red (add �200)");
				System.out.println("b. Green (add �300)");
				System.out.println("c. White (add �400)");
				details = input.next();
			}
			
			if(details.equals("a")) {
				cost += 200;
			}else if(details.equals("b")) {
				cost += 300;
			}else {
				cost += 400;
			}
		}
		
		System.out.println("Would you like heated seats?");
		System.out.println("y for yes");
		System.out.println("n for no");
		
		details = input.next();
		while(!details.equals("y") && !details.equals("n")) {
			System.out.println("ERROR: Invalid input.");
			System.out.println("Would you like heated seats?");
			System.out.println("y for yes");
			System.out.println("n for no");
			
			details = input.next();
		}
		
		if(details.equals("y")) {
			cost += 500;
		}
		
		System.out.println("Would you like air conditioning?");
		System.out.println("y for yes");
		System.out.println("n for no");
		
		details = input.next();
		while(!details.equals("y") && !details.equals("n")) {
			System.out.println("ERROR: Invalid input.");
			System.out.println("Would you like air conditioning?");
			System.out.println("y for yes");
			System.out.println("n for no");
			
			details = input.next();
		}
		
		if(details.equals("y")) {
			cost += 500;
		}
		
		System.out.println("Would you like parking sensors?");
		System.out.println("y for yes");
		System.out.println("n for no");
		
		details = input.next();
		while(!details.equals("y") && !details.equals("n")) {
			System.out.println("ERROR: Invalid input.");
			System.out.println("Would you like parking sensors?");
			System.out.println("y for yes");
			System.out.println("n for no");
			
			details = input.next();
		}
		
		if(details.equals("y")) {
			cost += 100;
		}
		
		System.out.println("The total cost of your car is �" + cost + ".00");
	}

}
