package Tasks;

import java.util.Scanner;

public class CircularMotion {

	public static void main(String[] args) {
		double mass, length, maxTension, speed, usersTension;
		
		maxTension = 60;
		mass = 2;
		length = 3;
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("What is your rotation speed? ");
		speed = input.nextDouble();
		
		usersTension = (mass * speed * speed)/length;
		
		if(usersTension > maxTension) {
			System.out.println("This speed will cause the rope to break.");
		}else {
			System.out.println("This speed is safe.");
		}
	}

}
