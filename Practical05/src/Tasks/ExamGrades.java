package Tasks;

import java.util.Scanner;

public class ExamGrades {

	public static void main(String[] args) {
		int percentage;
		String details;
		
		Scanner input = new Scanner(System.in);
		details = "";
		System.out.println("What did you get in the exam?");
		if(input.hasNextInt()) {
			percentage = input.nextInt() / 10;
			details += "Your grade is: ";
			switch(percentage) {
			case 1:
			case 2:
			case 3: details += "U";
				break;
			case 4: details += "E";
			break;
			case 5: details += "D";
			break;
			case 6: details += "C";
			break;
			case 7: details += "B";
			break;
			case 8: details += "A";
			break;
			case 9:
			case 10: details += "A*";
			break;
			default: details = "ERROR: invalid percentage.";
			}
			details += "\nNote: A minimum grade of C is required to pass.";
			System.out.println(details);
			
		}else {
			System.out.println("ERROR: Must input a number value");
		}

	}

}
