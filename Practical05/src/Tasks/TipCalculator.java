package Tasks;

import java.util.Scanner;

public class TipCalculator {  
	public static void main(String[] args) { 
		//CREATE NEW SCANNER OBJECT HERE 
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Do you want to leave a tip? Enter y (yes) or n (no)");  
		String choice = sc.nextLine();  
		
		while(!choice.equals("y") && !choice.equals("n")) {
			System.out.println("ERROR: Invalid Input.");
			System.out.println("Do you want to leave a tip? Enter y (yes) or n (no)");  
			choice = sc.nextLine();  
		}
			
		double bill, tip;      
		//ADD IF STATEMENT HERE TO CHECK IF THE USER INPUT WAS EQUAL TO y  
		if(choice.equals("y")){   
			System.out.println("Please enter your bill total");    
			bill = sc.nextDouble(); 
			//ADD IF STATEMENT HERE TO CHECK IF THE BILL TOTAL THE USER ENTERED 
			// WAS MORE THAN OR EQUAL TO �20  
			if(bill >= 20){ 
				System.out.println("Your bill is more than or equal to �20. A 20% tip is appropriate"); 
				//WORK OUT WHAT 20% OF THE BILL IS AND PLACE RESULT IN TIP VARIABLE 
				tip = bill * .2;
				//PRINT TIP TO 2 DECIMAL PLACES HERE  
				System.out.println("Your tip amount is: " + String.format("�%.2f", tip));  
				//ADD TIP TO BILL VARIABLE HERE 
				bill += tip;
				System.out.printf("You total bill including tip is: �%.2f" , bill);  
				} else {
					System.out.println("Your bill under �20. A 10% tip is appropriate"); 
					//WORK OUT WHAT 10% OF THE BILL IS AND PLACE RESULT IN TIP VARIABLE 
					tip = bill * .1;
					//PRINT TIP TO 2 DECIMAL PLACES HERE 
					System.out.println("Your tip amount is: " + String.format("�%.2f", tip)); 
					//ADD TIP TO BILL VARIABLE HERE 
					System.out.printf("Your total bill including tip is: �%.2f" , bill);
					}  
				} else {    
					System.out.println("You do not wish to calculate a tip");  
				}  
		sc.close();
				} 
	
			
		
	}
