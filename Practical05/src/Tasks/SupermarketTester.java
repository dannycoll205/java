package Tasks;

import java.util.Scanner;

public class SupermarketTester {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int number = 0;
		Supermarket shop1 = new Supermarket(0);
		Scanner input = new Scanner(System.in);
		
		String loyalty, voucherChoice, voucherType;
		boolean invalid = true;
		do {
			System.out.println("How many items do you want to scan?");
		
			if(!input.hasNextInt()) {
				System.out.println("ERROR");
				input.nextLine();
			}else {
				number = input.nextInt();
				invalid = false;
			}
		
		}while(invalid);
		shop1.scan(number);
		System.out.println("Does the customer have a loyalty card? Enter y for yes, n for no.");
		loyalty = input.next();
		
		while(!loyalty.equals("y") && !loyalty.equals("n")) {
			System.out.println("ERROR: Invalid Input.");
			System.out.println("Does the customer have a loyalty card? Enter y for yes, n for no.");
			loyalty = input.next();
		}
		
		if(loyalty.equals("y")) {
			shop1.discounts("LoyaltyCard");
		}
		
		System.out.println("Does the customer have any vouchers they want to use? Enter y for yes, n for no.");
		voucherChoice = input.next();
		
		while(!voucherChoice.equals("y") && !voucherChoice.equals("n")) {
			System.out.println("ERROR: Invalid Input.");
			System.out.println("Does the customer have any vouchers they want to use? Enter y for yes, n for no.");
			voucherChoice = input.next();
		}
		
		if(voucherChoice.equals("y")) {
			System.out.println("Please enter 5 if they would like to use a �5 voucher or 10 if they would like to use a �10 voucher.");
			voucherType = input.next();
			
			while(!voucherType.equals("5") && !voucherType.equals("10")) {
				System.out.println("ERROR: Invalid Input.");
				System.out.println("Please enter 5 if they would like to use a �5 voucher or 10 if they would like to use a �10 voucher.");
				voucherType = input.next();
			}
			
			if(voucherType.equals("5")) {
				shop1.discounts("VoucherFive");
			}else {
				shop1.discounts("VoucherTen");
			}
		}
		System.out.printf("The total bill including discounts is: �%.2f", shop1.getBill());
	}

}
