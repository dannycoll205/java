package Tasks;

import java.util.Scanner;

public class Microwave {
	
	public static void main(String[] args) {
		int numberOfItems;
		double minutes;
		Scanner input = new Scanner(System.in);
		
		System.out.println("How many items are you heating?");
		numberOfItems = input.nextInt();
		
		System.out.println("What is the heating time?");
		minutes = input.nextInt();
		
		switch(numberOfItems) {
		case 1: oneItemTime(minutes);
		break;
		case 2: twoItemTime(minutes);
		break;
		case 3: threeItemTime(minutes);
		break;
		default: System.out.println("Heating more than 3 items at once is not recommended.");
		}
	}
	
	public static double oneItemTime(double minutes) {
		System.out.println("Set time to: " + minutes);
		return minutes;
	}
	
	public static double twoItemTime(double minutes) {
		minutes = 1.5 * minutes;
		System.out.println("Set time to: " + minutes);
		return minutes;
	}
	
	public static double threeItemTime(double minutes) {
		minutes = 2 * minutes;
		System.out.println("Set time to: " + minutes);
		return minutes;
	}
}
