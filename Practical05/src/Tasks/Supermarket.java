package Tasks;

import java.util.Scanner;

public class Supermarket {
	private double bill, price;
	
	public Supermarket(double amount) {
		this.bill = amount;
	}
	public double scan(double times){
		for(int i = 1; i <= times; i++) {
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter Item Price:");
			price = sc.nextDouble();
			bill += price;
		}
		return bill;
	}
	
	public double discounts(String type) {
		if(type.equals("VoucherFive")) {
			bill -= 5;
		}else if(type.equals("VoucherTen")){
			bill -= 10;
		}else if(type.equals("LoyaltyCard")) {
			bill = bill * .9;
		}
		return bill;
	}
	
	public double getBill() {
		return bill;
	}
}
