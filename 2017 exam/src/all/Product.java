package all;

public class Product {
	private int proCode, proQtyAvailable;
	private String proMake, proModel;
	private double proPrice;
	private boolean proDiscontinued = false;
	
	public Product(int proCode, int proQtyAvailable, String proMake, String proModel, double proPrice, 
			boolean proDiscontinued) {
		this.proCode = proCode;
		this.proQtyAvailable = proQtyAvailable;
		this.proMake = proMake;
		this.proModel = proModel;
		this.proPrice = proPrice;
		this.proDiscontinued = proDiscontinued;
	}

	public int getProCode() {
		return proCode;
	}

	public void setProCode(int proCode) {
		this.proCode = proCode;
	}

	public int getProQtyAvailable() {
		return proQtyAvailable;
	}

	public void setProQtyAvailable(int proQtyAvailable) {
		this.proQtyAvailable = proQtyAvailable;
	}

	public String getProMake() {
		return proMake;
	}

	public void setProMake(String proMake) {
		this.proMake = proMake;
	}

	public String getProModel() {
		return proModel;
	}

	public void setProModel(String proModel) {
		this.proModel = proModel;
	}

	public double getProPrice() {
		return proPrice;
	}

	public void setProPrice(double proPrice) {
		this.proPrice = proPrice;
	}

	public boolean isProDiscontinued() {
		return proDiscontinued;
	}

	public void setProDiscontinued(boolean proDiscontinued) {
		this.proDiscontinued = proDiscontinued;
	}
	
	
}
