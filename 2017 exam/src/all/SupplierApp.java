package all;

import java.util.Scanner;

public class SupplierApp {
	
	public static String[] menuOptions = {"1. Print All Products", "2. Add Supplier", "3. Add Product", "4. Exit"};
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		boolean using = true;
		do {
			displayMenu();
			switch(getUserInput(input)) {
			case 1: printProducts();
					break;
			case 2: addSupplier();
					break;
			case 3: addProduct();
					break;
			case 4: using = false;
					break;
			default: System.out.println("Please enter a valid option.");
			}
		}while(using);
	}
	
	public static void displayMenu() {
		for(String eachOption:menuOptions) {
			System.out.println(eachOption);
		}
	}
	
	public static int getUserInput(Scanner input) {
		boolean invalid = true;
		int choice = -1;
		do {
			System.out.print("Enter a choice:");
			if(input.hasNextInt()) {
				choice = input.nextInt();
				invalid = false;
			}else {
				input.nextLine();
			}
		}while(invalid);
		return choice;
	}
	
	public static void printProducts() {
		
	}
	
	public static void addSupplier() {
		
	}
	
	public static void addProduct() {
		
	}
}
