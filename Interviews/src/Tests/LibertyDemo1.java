package Tests;
// you can also use imports, for example:
// import java.util.*;
// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

class Solution {
    public String solution(int A, int B, int C) {
        // write your code in Java SE 8
        String res="";
        int max=0;
        int strLength = (A+B+C);
        //if strLength is less than 3
        if(A<=1&&B<=1&&C<=1){
            while(A>0){
                res+="a";
                A--;
            }
            while(B>0){
                res+="b";
                B--;
            }
            while(C>0){
                res+="c";
                C--;
            }
            return res;
        }
        
        //determining which is the max & whether or not the largest value must be altered
        if(A>=B&&A>=C){
            if(A>(B+C)*2+2){
                A=(B+C)*2+2;
                strLength=A+B+C;
            }
            max=A;
            res+="aa";
            A-=2;
            
        }//if A is max
        else if(B>=A&&B>=C){
            if(B>(A+C)*2+2){
                B=(A+C)*2+2;
                strLength=A+B+C;
            }
            max=B;
            
            res+="bb";
            B-=2;
             
        }//if B is max
        else{
            if(C>(A+B)*2+2){
                C=(A+B)*2+2;
                strLength=A+B+C;
            }
            max=C;
    
            res+="cc";
            C-=2;
            
        }        //if C is max
        
        for(int i=2;i<strLength;i++){
            
            //reevaulate the max
            if(A>=B&&A>=C){max=A;}
            else if(B>=A&&B>=C){max=B;}
            else{max=C;}
            
            while(A>0||B>0||C>0){
                if(max==A){
                    if(res.charAt(i-1)==res.charAt(i-2)&&res.charAt(i-1)=='a'){
                        if(B>C){
                            res+="b";
                            B--;
                        }else{
                            res+="c";
                            C--;
                        }
                        break;
                    }
                    res+="a";
                    A--;
                    break;
                    
                }else if(max==B){
                    if(res.charAt(i-1)==res.charAt(i-2)&&res.charAt(i-1)=='b'){
                        if(A>=C){
                            res+="a";
                            A--;
                        }else{
                            res+="c";
                            C--;
                        }
                        break;
                    }
                    res+="b";
                    B--;
                    break;
                
                }else{
                    if(res.charAt(i-1)==res.charAt(i-2)&&res.charAt(i-1)=='c'){
                        if(A>=B){
                            res+="a";
                            A--;
                        }else{
                            res+="b";
                            B--;
                        }
                        break;
                }
	                res+="c";
	                C--;       
	                break;
                
                }   
            }
        }
        return res;
    
    }
       
    public Solution() {
    	
    }
}

//6,1,1
//1,3,1
//0,8,1
//between 0-100
//a+b+c>100
