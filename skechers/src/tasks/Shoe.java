package tasks;

public class Shoe {
	private int style, ukSize, usSize, plu;
	private double price;
	private String colour;
	
	public Shoe(int style, int size, int plu, double price, String colour) {
		this.style = style;
		this.ukSize = size-1;
		this.plu = plu;
		this.price = price;
		this.colour = colour;
	}
}
