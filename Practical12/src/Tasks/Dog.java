package Tasks;

public class Dog extends Canine implements Pet{

	public Dog(foodType food, int hunger, int age, String name, boolean sleepStatus, double weight) {
		super(food, hunger, age, name, sleepStatus, weight, Continents.Asia);
		// TODO Auto-generated constructor stub
	}
	
	public void makeNoise() { 
		System.out.println("I barked like a Dog"); 
	}
	
	public void eat() {
		System.out.println("I am a dog and have just eaten my " + getFood());
	}

	@Override
	public void beFriendly() {
		// TODO Auto-generated method stub
		System.out.println("As I am a pet dog I enjoy time with my family");
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("As I am a pet dog I enjoy playing fetch");
	}

	@Override
	public void average() {
		// TODO Auto-generated method stub
		
	}
}
