package Tasks;

public class Cat extends Feline implements Pet{

	public Cat(foodType food, int hunger, int age, String name, boolean sleepStatus, double weight) {
		super(food, hunger, age, name, sleepStatus, weight, Continents.Europe);
		// TODO Auto-generated constructor stub
	}
	
	public void makeNoise() {
		System.out.println("I meowed like a Cat");
	}
	
	public void eat(int amount) {
		super.eat(amount);
		System.out.println("I am a cat and have just eaten my "+getFood());
	}

	@Override
	public void beFriendly() {
		// TODO Auto-generated method stub
		System.out.println("As I am a pet cat I enjoy time with my family");
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("As I am a pet cat I enjoy playing with the toy mice");
	}

	@Override
	public void average() {
		// TODO Auto-generated method stub
		
	}
	
}
