package Tasks;

import java.util.ArrayList;

public class AnimalTester {

	public static void main(String[] args) {
		ArrayList<Animal> ark = new ArrayList<Animal>();
		Hippo henry = new Hippo(foodType.GRASSES, 50, 2,"Henry",false, 100);
		ark.add(henry);
		henry.eat(5);
		System.out.println();
		henry.makeNoise();
		System.out.println();
		henry.roam();
		System.out.println();
		
		System.out.println(henry.getName()+" the Hippo "+henry.sleep(10));
		
		Lion simba = new Lion(foodType.MEAT, 60,3,"Simba", false, 50);
		ark.add(simba);
		/*
		simba.eat();
		System.out.println();
		simba.makeNoise();
		System.out.println();
		simba.roam();
		System.out.println();
		System.out.println(simba.getName()+" the Lion "+simba.sleep(100));
		*/
		Cat perdo = new Cat(foodType.FISH, 50,15,"Perdo", false, 12);
		ark.add(perdo);
		Tiger stripes = new Tiger(foodType.MEAT, 100,4,"Stripes", false, 70);
		ark.add(stripes);
		/*
		perdo.eat(15);
		perdo.makeNoise();
		perdo.roam();
		/*
		stripes.eat();
		stripes.makeNoise();
		stripes.roam();
		
		perdo.beFriendly();
		perdo.play();
		*/
		findLocation(ark);
	}
	
	public static void findLocation(ArrayList<Animal> all) {
		int amount =0;
		for(Animal eachA:all) {
			if(eachA.getContinent().equals(Continents.Europe)) {
				System.out.print(eachA.getName()+" is in Europe. ");
				amount++;
			}
		}
		if(amount==0) {
			System.out.print("No animals are currently in Europe.");
		}
		System.out.println(amount);
		System.out.println();
	}
}
