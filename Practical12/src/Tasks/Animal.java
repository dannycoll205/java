package Tasks;

public abstract class Animal implements Ages{
	private foodType food;
	private int hunger, age;
	private String name;
	private boolean sleepStatus;
	private double weight;
	private Continents continent;
	
	
	public Animal(foodType food, int hunger, int age, String name, double weight, boolean sleepStatus, Continents contin) {
		
		this.food = food;
		this.hunger = hunger;
		this.age = age;
		this.name = name;
		this.weight = weight;
		this.sleepStatus = sleepStatus;
		this.setContinent(contin);
	}

	public foodType getFood() {
		return food;
	}

	public void setFood(foodType food) {
		this.food = food;
	}

	public int getHunger() {
		return hunger;
	}

	public void setHunger(int hunger) {
		this.hunger = hunger;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isSleepStatus() {
		return sleepStatus;
	}

	public void setSleepStatus(boolean sleepStatus) {
		this.sleepStatus = sleepStatus;
	}

	public String sleep(double time) {
		if(!this.sleepStatus) {
			this.sleepStatus = !this.sleepStatus;
			return " is sleeping and will be for " +time+ " minutes.";
		}else {
			return " is already sleeping, please wait until it wakens.";
		}
	}
	
	abstract void makeNoise();	
	
	public void eat(int amount) {
		if(amount>hunger) {
			System.out.println("I'm not hungry enough to eat that much "+getFood());
		}else {
			hunger-=amount;
			System.out.println("I just ate "+amount+" "+getFood()+". I still have "+getHunger()+" hunger.");
		}
	}
	abstract void roam();

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public Continents getContinent() {
		return continent;
	}

	public void setContinent(Continents continent) {
		this.continent = continent;
	}
	
	

}
