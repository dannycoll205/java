package Tasks;

public interface Pet {
	abstract void beFriendly();
	abstract void play();
}
