package Tasks;

public class Tiger extends Feline {

	public Tiger(foodType food, int hunger, int age, String name, boolean sleepStatus, double weight) {
		super(food, hunger, age, name, sleepStatus, weight, Continents.Asia);
		// TODO Auto-generated constructor stub
	}

	public void makeNoise() {
		System.out.println("I roared like a Tiger");
	}
	
	public void eat() {
		System.out.println("I am a tiger and have just eaten my "+getFood());
	}

	@Override
	public void average() {
		// TODO Auto-generated method stub
		
	}
}
