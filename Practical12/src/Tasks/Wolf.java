package Tasks;

public abstract class Wolf extends Canine {

	public Wolf(foodType food, int hunger, int age, String name, boolean sleepStatus, double weight) {
		super(food, hunger, age, name, sleepStatus, weight, Continents.Europe);
		// TODO Auto-generated constructor stub
	}
	public void makeNoise() {  
		System.out.println("I howled like a Wolf"); 
	} 
	 
	public void eat() {
		System.out.println("I am a wolf and have just eaten my " + getFood()); 
	}
}
