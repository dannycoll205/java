package Tasks;

public class Lion extends Feline {

	public Lion(foodType food, int hunger, int age, String name, boolean sleepStatus, double weight) {
		super(food, hunger, age, name, sleepStatus, weight, Continents.Africa);
		// TODO Auto-generated constructor stub
	}
	
	@Override 
	void makeNoise() { 
		System.out.println("I roared like a Lion"); 
	}
	
	public void eat() {  
		System.out.println("I am a lion and have just eaten my " + getFood()); 
	}

	@Override
	public void average() {
		// TODO Auto-generated method stub
		
	}
}
