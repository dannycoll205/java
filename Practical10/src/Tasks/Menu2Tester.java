package Tasks;

import java.util.Scanner;

public class Menu2Tester {
	
	public static Scanner sc=new Scanner(System.in);
	
	public static void main(String[] args) {
		
		String[] mainMenu = {"Starters", "Mains", "Desserts"};
		
		String[] starterChoices= {"Rosemary Chicken", "Beef Wellington", "Maine Lobster"};
		String customerChoice = "";
		Menu2 starterMenu = new Menu2(starterChoices);
		try {
			CustomerMenu2Choice starter = new CustomerMenu2Choice(starterMenu, sc);
			customerChoice = starter.getCustomerChoice();
		}catch(MenuException me) {
			customerChoice = me.getMessage();
		}catch(ArrayIndexOutOfBoundsException aioobe) {
		
			customerChoice  ="an invalid array index out of bounds exception from the "+"Starter Menu";
		}
		System.out.println("You chose " + customerChoice);
		
		String[] dessertChoices= {"Banoffee", "Sticky Toffee", "Apple Crumble", "Cheese Board"};
		Menu2 dessertMenu = new Menu2(dessertChoices);
		try {
			CustomerMenu2Choice dessert = new CustomerMenu2Choice(dessertMenu, sc);
			customerChoice = dessert.getCustomerChoice();
		}catch(MenuException me) {
			customerChoice = me.getMessage();
		}catch(ArrayIndexOutOfBoundsException aioobe) {
			customerChoice = "an invalid array index out of bounds exception from the "+"Dessert Menu ";
		}
		System.out.println("You chose "+customerChoice);
		
	}
	
	public static void operateMainMenu(Scanner sc) {
		
	}

}
