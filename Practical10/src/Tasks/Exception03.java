package Tasks;

public class Exception03 { 
	public static void main(String[] args) { 
		try{
			int[] myArray = new int[10]; 
			System.out.println(myArray[10]);  
		}catch(ArrayIndexOutOfBoundsException e) {
			System.out.println("You are trying to access part of a string that doesn't exist "+e.getMessage());
		}
		System.out.println("... Execution of the rest of the code ... "); 
			}
} 
