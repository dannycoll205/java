package Tasks;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Menu2 {
	private String[] menuItems; 
	private char[] letterMenuOptions;
	
	public Menu2(String[] menuItems) { 
		this.menuItems = menuItems;
		this.letterMenuOptions = new char[menuItems.length];
	}  
	
	public String displayMenuItems(Scanner sc) throws MenuException{
		
		System.out.println("Type your selection, then press Enter."); 
		for (int i = 0; i < menuItems.length; i++) {  
			System.out.println((i+1) + " for " + menuItems[i]); 
			letterMenuOptions[i] = menuItems[i].charAt(0);
		}
		String choiceInput = sc.nextLine();
		for(int j=0;j<menuItems.length;j++) {
			if(choiceInput.toUpperCase().charAt(0)==letterMenuOptions[j]) {
				throw(new MenuException(menuItems[j]));
			}
		}
		int choice = Integer.parseInt(choiceInput);   
		return menuItems[choice-1]; 
		
	} 
	 
	public String[] getMenuItems() {  
		 return menuItems; 
	} 
	 
	public void setMenuItems(String[] menuItems) { 
		this.menuItems = menuItems; 
	}
	
	public char[] getLetterMenuOptions() {
		return letterMenuOptions;
	}
	
	public void setLetterMenuOptions(char[] letterOptions) {
		this.letterMenuOptions= letterOptions;
	}
} 

