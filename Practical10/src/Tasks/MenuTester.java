package Tasks;

import java.util.Scanner;

public class MenuTester {
	
	public static Scanner sc=new Scanner(System.in);
	
	public static void main(String[] args) {
		
		String[] starterChoices= {"Rosemary Chicken", "Beef Wellington", "Maine Lobster"};
		String customerChoice = "";
		Menu starterMenu = new Menu(starterChoices);
		try {
			CustomerMenuChoice starter = new CustomerMenuChoice(starterMenu, sc);
			customerChoice = starter.getCustomerChoice();
		}catch(ArrayIndexOutOfBoundsException aioobe) {
			customerChoice  ="an invalid array index out of bounds exception from the "+"Starter Menu";
		}
		System.out.println("You chose " + customerChoice);
		
		String[] dessertChoices= {"Banoffee", "Sticky Toffee", "Apple Crumble", "Cheese Board"};
		Menu dessertMenu = new Menu(dessertChoices);
		try {
			CustomerMenuChoice dessert = new CustomerMenuChoice(dessertMenu, sc);
			customerChoice = dessert.getCustomerChoice();
		}catch(ArrayIndexOutOfBoundsException aioobe) {
			customerChoice = "an invalid array index out of bounds exception from the "+"Dessert Menu ";
		}
		System.out.println("You chose "+customerChoice);
		
	}

}
