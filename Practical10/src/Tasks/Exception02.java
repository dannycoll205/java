package Tasks;

public class Exception02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			String name = "Fred";  
			System.out.println(name.charAt(10));  
		}catch(StringIndexOutOfBoundsException e) {
			System.out.println("You are trying to access part of a string that doesn't exist "+e.getMessage());
		}
		System.out.println("... Execution of the rest of the code ... ");
	}

}
