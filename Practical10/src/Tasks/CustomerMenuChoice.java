package Tasks;

import java.util.Scanner;

public class CustomerMenuChoice {
	private Menu menuItems;
	private String customerChoice;
	
	public CustomerMenuChoice(Menu theMenu, Scanner sc) {
		menuItems = theMenu;
		setCustomerChoice(sc);
	}
	
	public Menu getMenuItems() {
		return menuItems;
	}
	
	public String getCustomerChoice() {
		return this.customerChoice;
	}
	
	public void setMenuItems(Menu menuItems) {
		this.menuItems = menuItems;
	}
	
	public void setCustomerChoice(Scanner sc) {
		System.out.println("Choose from the following menu: ");
		this.customerChoice=menuItems.displayMenuItems(sc);
	}
}
