package Tasks;

import java.util.Scanner;

public class CustomerMenu2Choice {
	private Menu2 menuItems;
	private String customerChoice;
	
	public CustomerMenu2Choice(Menu2 theMenu, Scanner sc) throws MenuException {
		menuItems = theMenu;
		setCustomerChoice(sc);
	}
	
	public Menu2 getMenuItems() {
		return menuItems;
	}
	
	public String getCustomerChoice() {
		return this.customerChoice;
	}
	
	public void setMenuItems(Menu2 menuItems) {
		this.menuItems = menuItems;
	}
	
	public void setCustomerChoice(Scanner sc) throws MenuException{
		System.out.println("Choose from the following menu: ");
		this.customerChoice=menuItems.displayMenuItems(sc);
	}
}
