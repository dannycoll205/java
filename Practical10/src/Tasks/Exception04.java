package Tasks;

import java.util.ArrayList;

public class Exception04 {

	public static void main(String[] args) {
		try {
			ArrayList<String> numbers = new ArrayList<String>();
			numbers.get(5);
		}catch(IndexOutOfBoundsException e) {
			System.out.println("You are trying to access part of the ArrayList that doesn't exist "+e.getMessage());
		}
		System.out.println("... Execution of the rest of the code ... "); 

	}

}
