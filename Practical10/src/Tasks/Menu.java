package Tasks;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Menu {
	private String[] menuItems; 
	public Menu(String[] menuItems) { 
		this.menuItems = menuItems; 
		}  
	
	public String displayMenuItems(Scanner sc) { 
		System.out.println("Type your selection, then press Enter."); 
		for (int i = 0; i < menuItems.length; i++) {   
			System.out.println((i+1) + " for " + menuItems[i]); 
		}
		try {
			int choice = sc.nextInt();   
			return menuItems[choice-1]; 
		} catch (InputMismatchException ime) { 
			return "you have to select an option from the menu as a number"; 
		}
	} 
	 
	public String[] getMenuItems() {  
		 return menuItems; 
	} 
	 
	public void setMenuItems(String[] menuItems) { 
		this.menuItems = menuItems; 
	}
} 

