package Tasks;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Exception01 {  
	public static void main(String[] args) {   
		Scanner input = new Scanner(System.in);
		try{    
			//System.out.println("<<Entered the try block>>"); 
			
			int num1 = input.nextInt();   
			int num2 = input.nextInt();
			
			int sum = num1/num2; 
			System.out.println(sum);  
		}catch(InputMismatchException | ArithmeticException e) {  
		
			//System.out.println("<<Entered the catch block>>");   
			System.out.println("There is an arithmetic error in the app caused by: "      + e.getMessage());  
		}
		System.out.println(".. Execution of the rest of the code ..");  
		 
	}

}
