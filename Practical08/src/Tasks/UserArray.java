package Tasks;

import java.util.Scanner;

public class UserArray {

	public static void main(String[] args) {
		int numbers[] = new int[5];
		Scanner input = new Scanner(System.in);
		
		System.out.println("Enter 5 integers");
		int index = 0;
		int sum = 0;
		boolean invalid = true;
		//check input is valid
		do {
			do{	
				if(!input.hasNextInt()) {
					System.out.println("ERROR");
					input.nextLine();
				}else {
					//assign to array if integer inputed
					invalid = false;
					numbers[index] = input.nextInt();
					index++;
				}
				}while(invalid);
		}while(index<5);
		
		input.close();
		//print array
		for(int i = 0; i<5; i++) {
			System.out.print(numbers[i] + " ");
			sum += numbers[i];
		}
		System.out.println();
		System.out.println("The sum of the numbers is: " + sum);
		
		//print in reverse
		System.out.print("The numbers in reverse order are: ");
		for(int i = 4; i>=0; i--) {
			System.out.print(numbers[i] + " ");
		}
		
	}

}
