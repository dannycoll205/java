package Tasks;

import java.util.ArrayList;
import java.util.Scanner;

public class Validation {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		ArrayList<Thingy> bitsAndPieces = new ArrayList<Thingy>();
		
		Thingy thy1 = new Thingy(21, "FSNs;krs");
		Thingy thy2 = new Thingy(22, "cnopegvw");
		
		bitsAndPieces.add(thy1);
		bitsAndPieces.add(thy2);
		
		System.out.println(bitsAndPieces);
		
		for(int i = 0; i<bitsAndPieces.size(); i++) {
			Thingy th = bitsAndPieces.get(i);
			th.kerfuffle();
		}
		
		String result = firstLast(bitsAndPieces);
		System.out.println(result);
		
		System.out.println("String: ");
		String using = input.nextLine();
		System.out.println("Position: ");
		int choice = checkUserInput(input);
		
		input.close();
		
		System.out.println(getChar(using, choice-1));
		
	}
	
	public static char getChar(String str, int index) {
		if(str != null && !str.equals("") && index >= 0 && index < str.length()) {
			return str.charAt(index);

		}
		return '?';
	}
	
	public static int checkUserInput(Scanner input) {
		int res = -1;
		boolean invalid = true;
		do {
			if(input.hasNextInt()) {
				res = input.nextInt();
				invalid = false;
			}else {
				System.out.println("Error input a number.");
			}
		}while(invalid);
		return res;
	}
	
	public static String firstLast(ArrayList<Thingy> all) {
		String res  = "";
		res += all.get(0) + " ";
		res += all.get(all.size()-1);
		return res;
	}
}
