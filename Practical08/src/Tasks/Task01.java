package Tasks;

import java.util.Scanner;

public class Task01 { 
	public static void main(String[] args) {  
		//Set up the scanner for input  
		Scanner sc = new Scanner(System.in);
		
		//create an array to hold 5 integer elements
		int[] intArray = new int[5]; 
		
		//call the getUserInput method and save the temporary array to the actual array 
		intArray = getUserInput(sc);   
		
		//calculate the sum of the elements in the array  
		int sum = 0;  
		for (int element:intArray) {  
			System.out.print(element + "\t");  
			sum += element;  
		}   
		System.out.println("\nThe sum of the numbers is: " + sum);    
		
		//print out the elements of the array in reverse order  
		System.out.print("The numbers in reverse order are: ");  
		for (int i = intArray.length-1; i >= 0; i--) {  
			System.out.print(intArray[i] + "\t");  
		}
		sc.close(); //close the scanner object 
	}   
	//The getUserInput method to accept and validate user input of five integers. 
	public static int[] getUserInput(Scanner sc) {  
		boolean validInput = false; 		//a flag set to false (assuming user input will not be valid)  
		int[] tempArray = new int[5];		//setting up a temporary array  
		int userInput;		//setting up an integer variable to hold user input    
		
		//a loop to add all five numbers to the temporary array  
		for (int i = 0; i < 5; i++) {   
			//a do while loop to validate user input  
			do {    
				validInput = false; //ensure when the loop repeats that the flag is false
				System.out.print("Please enter an integer: ");    
				if (sc.hasNextInt()) { 
					//checking if an integer has been entered   
					//if an integer has been entered      
					userInput = sc.nextInt(); //save the integer to the variable    
					sc.nextLine(); //you need to push the Scanner to read a line to allow 
					//for further input     
					tempArray[i] = userInput; // save the integer variable into the next position in the
					//array
					validInput = true;  //set the flag to true (i.e. at this stage the user has 
					//entered valid data     
				} else {
					//if an integer has NOT been entered     
					System.out.println("Please enter valid input"); //Error message to the user  
					sc.nextLine(); //you need to push the Scanner to read a line to allow for further 
					//input    
				}
			} while (!validInput);    
		}   
		return tempArray; //return the temporary array to the main method 
	} 
					 
}
