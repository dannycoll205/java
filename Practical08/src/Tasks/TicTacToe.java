package Tasks;

import java.util.Scanner;

public class TicTacToe {

	static char token[][] = new char[3][3];
	static boolean xWin = false;
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int turncount = 0;
		int k =0;
		int choice = 0;
		char count[] = {'1','2','3','4','5','6','7','8','9'};
		//set up board
		for(int i = 0; i<3; i++) {
			for(int j = 0; j<3; j++) {
				
				token[i][j] = count[k];
				k++;
			}
			System.out.println();
		}
		printGame();
		//find users choice
		do {
			//validate choice
			boolean invalid = true;
			do{
				System.out.println("Select Space");
				if(input.hasNextInt()) {
					choice = input.nextInt();
					//make sure input is less than 9
					if(choice <=9) {
						invalid = false;
					}else {
						System.out.println("Error: Must be between 1 and 9.");
						input.nextLine();
					}
				}else {
					System.out.println("Error: Input must be a number between 1 and 9.");
					input.nextLine();
				}
			}while(invalid);
			
			//ensure number not already taken
			if(token[(choice-1)/3][(choice-1)%3] != 'X' && token[(choice-1)/3][(choice-1)%3] != 'O') {
				if(turncount%2==0) {
					token[(choice-1)/3][(choice-1)%3] = 'X';
				}else {
					token[(choice-1)/3][(choice-1)%3] = 'O';
				}
				turncount++;
			}else {
				System.out.println("Spot already taken.");
			}
			printGame();
		}while(turncount<9 && !checkWin());
		
		System.out.println("Game Over");
		if(!checkWin()) {
			System.out.print("Draw");
		}
		if(xWin) {
			System.out.println("X Wins!");
		}else {
			System.out.println("O Wins!");
		}
	}
	public static boolean checkWin() {
		boolean won = false;
		//loop for horiz. & vert.
		for(int i=0; i<3; i++) {			
			if(token[i][0] == token[i][1] && token[i][0] == token[i][2]) {
				won = true;
				if(token[i][0] == 'X') {
					xWin = true;
				}
			}
			if(token[0][i] == token[1][i] && token[0][i] == token[2][i]) {
				won = true;
				if(token[i][0] == 'X') {
					xWin = true;
				}
			}
		}
		//check diagonals
		if(token[0][0] == token[1][1] && token[0][0] == token[2][2]) {
			won = true;
			if(token[1][1] == 'X') {
				xWin = true;
			}
		}
		if(token[0][2] == token[1][1] && token[2][0] == token[1][1]) {
			won = true;
			if(token[1][1] == 'X') {
				xWin = true;
			}
		}
		
		return won;
	}
	public static void printGame() {
	
		for(int i = 0; i<3; i++) {
			for(int j = 0; j<3; j++) {
				System.out.print("  " + token[i][j] +"  |");
			}
			System.out.println();
			if(i<2) {
				System.out.println("-------------------");
			}
		}
	}
}
