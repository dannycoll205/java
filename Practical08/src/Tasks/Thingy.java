package Tasks;

public class Thingy {
	private int dunno;
	private String what;
	
	public Thingy(int d, String w) {
		this.dunno = d;
		this.what = w;
	}
	
	public String toString() {
		String res = dunno + "? " + what;
		return res;
	}
	
	public void kerfuffle() {
		dunno = 0;
		what = "ZZZ";
	}
}
