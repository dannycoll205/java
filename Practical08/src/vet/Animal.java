package vet;

public class Animal {
	private String name, owner;
	private int age;
	private AnimalType type;
	public Animal(String name, String owner, int age, AnimalType type) {
		this.name = name;
		this.owner = owner;
		this.age = age;
		this.type = type;
	}
	public String getName() {
		return name;
	}
	
	public AnimalType getType() {
		return type;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	};
	
	
}
