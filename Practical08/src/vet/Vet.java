package vet;

import java.util.ArrayList;

public class Vet {
	private int id;
	private String name;
	private ArrayList<Animal> animals = new ArrayList<Animal>();
	public Vet(int id, String name, ArrayList<Animal> animals) {
		this.id = id;
		this.name = name;
		this.animals = animals;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<Animal> getAnimals() {
		return animals;
	}
	public void setAnimals(ArrayList<Animal> animals) {
		this.animals = animals;
	}
	
	//methods
	public void printAllVetDetails() {
		
		System.out.println("VET DETAILS");
		System.out.println("=======================");
		System.out.println("Vet's ID: " + this.id);
		System.out.println("Vet's Name: " + this.name);
		System.out.println(this.name + " is responsible for ");
		for(Animal eachAnimal:this.getAnimals()) {
			System.out.println("Animal name: " + eachAnimal.getName());
			System.out.println("Animal owner: "+ eachAnimal.getOwner());
			System.out.println("Animal age: "+ eachAnimal.getAge());
			System.out.println("Animal type: "+ eachAnimal.getType());
			System.out.println("--------------------");
		}
				
	}
	
	public void searchVet(String searchName) {
		if(searchName.equalsIgnoreCase(getName())) {
			System.out.println("The vet " + searchName + " cares for: ");
			for(int i = 0; i< getAnimals().size(); i++) {
				System.out.println(getAnimals().get(i).getName() + ": " + getAnimals().get(i).getType());
			}
		}
	}
	
	public void addAnimal(String name, String owner, int age, AnimalType type, ArrayList<Animal> animals) {
		animals.add(new Animal(name, owner, age, type));		
	}
	
}
