package vet;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class vetTester {
	
	static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		ArrayList<Vet> allVets = new ArrayList<Vet>();
		allVets = createVetArrayList();
		boolean finished;
		
		for(Vet eachVet:allVets) {
			eachVet.printAllVetDetails();
		}
		do {
			finished = false;
		
			printMenu();
			
			int menuOption = checkUserInput(input);
			switch (menuOption) {
			case 1: searchAnimal(input, allVets);
				break;
			case 2: System.out.println("Please enter the name of the vet: ");
					String searchName = input.nextLine();
					for(Vet eachVet:allVets) {
						eachVet.searchVet(searchName);
					}
				break;
			case 3: addNewVet(allVets);
				break;
			case 4: addAnimalToVet(allVets);
				break;
			case 5: finished = true;
				break;
			default: System.out.println("Please enter a valid menu option.");
					break;
			}
		}while(!finished);
		input.close();
	}
	
	public static ArrayList<Animal> createAnimalArrayList(){
		String animalName[] = {"Skye", "Toby", "Peppa","Charley", "Nemo", "Maisie"};
		String ownerName[] = {"Joe Bloggs", "Mary Rice", "Ann Carroll", "Ciara Roddy", "Lillian Parks", "Ruth Jones"};
		int animalAge[] = {10, 5, 1, 6, 2, 8, 3};
		AnimalType animalType[] = {AnimalType.CAT, AnimalType.DOG, AnimalType.COW, AnimalType.GERBIL, AnimalType.HORSE, AnimalType.SHEEP};
		
		ArrayList<Animal> animalList = new ArrayList<Animal>();
		Random rand = new Random();
		int randomNum = 0;
		String aName = "";
		String oName = "";
		int aAge = -1;
		AnimalType aType = null;
		
		int randNumAnimals = rand.nextInt(4)+1;
		
		for(int i = 0; i < randNumAnimals; i++) {
			aName = animalName[rand.nextInt(6)];
			oName = ownerName[rand.nextInt(6)];
			aAge = animalAge[rand.nextInt(6)];
			
			aType = animalType[rand.nextInt(6)];
			animalList.add(new Animal(aName, oName, aAge, aType));
			
		}
		return animalList;
	}
	
	public static ArrayList<Vet> createVetArrayList(){
		int vetID[] = {123,345,456,567,12};
		String vetName[] = {"Tara Brown", "Danny Piper", "Jack Smith", "Amy Owens", "a"};
		
		int vID = -1;
		String vName = null;
		
		ArrayList<Vet> vets = new ArrayList<Vet>();
		for(int i = 0; i<5; i++) {
			vID = vetID[i];
			vName = vetName[i];
			vets.add(new Vet(vID, vName, createAnimalArrayList()));
		}
		return vets;
	}
	
	public static void addNewVet(ArrayList<Vet> vets) {
		System.out.println("Enter the vets name: ");
		String name = input.nextLine();
		System.out.println("Enter ID: ");
		int id = checkUserInput(input);
		vets.add(new Vet(id, name, createAnimalArrayList()));
		
	}
	
	public static void printMenu() {
		System.out.println("Vetinary Surgery Application");
		System.out.println("============================");
		System.out.println("1. Search for Animal");
		System.out.println("2. Search for Vet");
		System.out.println("3. Add a Vet");
		System.out.println("4. Add an Animal");
		System.out.println("5. Exit System");
		
		System.out.println("Enter Option: ");
	}
	
	public static void searchAnimal(Scanner input, ArrayList<Vet> allVets) {
		System.out.println("Please enter the name of the animal: ");
		String searchName = input.nextLine();
		System.out.println("Please enter the owners name: ");
		String searchOwner = input.nextLine();
		for(int i = 0; i<allVets.size(); i++) {
			for(int j = 0; j<allVets.get(i).getAnimals().size(); j++) {
				if(searchName.equalsIgnoreCase(allVets.get(i).getAnimals().get(j).getName()) && 
						searchOwner.equalsIgnoreCase(allVets.get(i).getAnimals().get(j).getOwner())) {
					System.out.println("The vet for " + searchOwner+"'s "+ searchName + " is " + allVets.get(i).getName());
				}
			}
		}
		
	}

	public static int checkUserInput(Scanner input) {
		boolean invalid = true;
		int menuOption = 0;
		do {
			invalid = true;
			if(input.hasNextInt()) {
				menuOption = input.nextInt();
				input.nextLine();
				invalid = false;
			}else {
				System.out.println("Please enter a valid menu option.");
				input.nextLine();
			}
		}while(invalid);
		return menuOption;
	}
	
	public static void addAnimalToVet(ArrayList<Vet> allVets) {
		System.out.println("Who will care for this animal?");
		String searchName = input.nextLine();
		Vet v = null;
		
		for(int i = 0; i<allVets.size(); i++) {
			if(searchName.equalsIgnoreCase(allVets.get(i).getName())){
				v = allVets.get(i);
				break;
			}
		}
		
		AnimalType animalType[] = {AnimalType.CAT, AnimalType.DOG, AnimalType.COW, AnimalType.GERBIL, AnimalType.HORSE, AnimalType.SHEEP};
		AnimalType type = null;
		System.out.println("What is the animal's name?");
		String name = input.nextLine();
		System.out.println("What is the owner's name?");
		String owner = input.nextLine();
		System.out.println("How old is " + name +"?");
		int age = checkUserInput(input);
		System.out.println("Is " + name+" a ");
		for(int i = 0; i < 6; i++) {
			System.out.print((i+1) + ") " + animalType[i] +"\t");
		}
		System.out.println();
		int choice = checkUserInput(input);
		type = animalType[choice];
		
		v.addAnimal(name, owner, age, type, v.getAnimals());
	}
}
