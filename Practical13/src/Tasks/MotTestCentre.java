package Tasks;

public class MotTestCentre {
	
	private String centreName;
	private int noPass, noFail, totalTested;
	private double passRate, failRate;
	
	
	public String getCentreName() {
		return centreName;
	}
	public void setCentreName(String centreName) {
		this.centreName = centreName;
	}
	public int getNoPass() {
		return noPass;
	}
	public void setNoPass(int noPass) {
		this.noPass = noPass;
	}
	public int getNoFail() {
		return noFail;
	}
	public void setNoFail(int noFail) {
		this.noFail = noFail;
	}
	
	
	public MotTestCentre(String centreName, int noPass, int noFail) {
		this.centreName = centreName;
		this.noPass = noPass;
		this.noFail = noFail;
		this.totalTested=noPass+noFail;
		this.passRate=(double)noPass/totalTested *100;
		this.failRate=(double)noFail/totalTested *100;
	}
	
	public String printStats() {
		String res = "";
		res +=(centreName+" passed "+noPass+" cars and failed "+noFail);
		res+=(". A total of "+totalTested+" were tested with a ");
		res+=(String.format("%.2f%% pass rate\n", passRate));
		return res;
	}
	public double getPassRate() {
		// TODO Auto-generated method stub
		return passRate;
	}
	public int getTotalTested() {
		// TODO Auto-generated method stub
		return totalTested;
	}
	public double getFailRate() {
		// TODO Auto-generated method stub
		return failRate;
	}
	
	public String writeToCsv() {
		String res = centreName + ", ";
		/**res+=totalTested + ", "; 
		res+=noPass + ", "; 
		res+=noFail+ ", "; **/
		res+=this.printStats();
		res+=String.format("%.2f, ",passRate); 
		res+=String.format("%.2f\n",failRate);
		
		return res;
	}
}
