package Tasks;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MotRead1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		boolean hasHeader = true;
		String path = "C:/test/motNiPrivateCars2015-16.csv";

		try {
			File myF = new File(path);
			Scanner in = new Scanner(myF);
			if(hasHeader) {
				in.nextLine();
			}
			while(in.hasNextLine()) {
				String record = in.nextLine();
				String[] parts = record.split(",");
				
				String centreName = parts[0].trim();
				int noPassed = Integer.parseInt(parts[1].trim());
				int noFailed = Integer.parseInt(parts[2].trim());
				
				System.out.println(centreName + " passed "+noPassed+" cars and failed "+noFailed);
			}
			
			in.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

}
