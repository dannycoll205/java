/**
 * 
 */
package Tasks;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * @author dptc9
 *
 */
public class WriteTextFile {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String myDir = "C:/test/output1.txt";
		try {
			PrintWriter myPw = new PrintWriter(myDir);
			//Print hello world to file
			myPw.println("Hello World!"); 
			
			//Define and print some formatted text to be printed to file 
			double pi = 3.14159;
			myPw.printf("Fact of the day, pi to two decimal places is %.2f", pi);
			
			myPw.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
