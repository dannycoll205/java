package Tasks;

import java.util.ArrayList;

public class MotTester {
	static String csvPath = "C:/test/motNiPrivateCars2015-16.csv", csvOutPath="C:/test/CarMotStats.csv";
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<MotTestCentre> myCentres=new ArrayList<MotTestCentre>();
		myCentres.addAll(MotRead2.importMotData(csvPath, true));
		for(MotTestCentre each:myCentres) {
			System.out.println(each.printStats());
		}
		
		MotWrite.exportStats(myCentres, csvOutPath);
		
	//	MotWrite.appendInfo(makeNewCentre("Hi",1,2, myCentres), csvPath);
		
	}
	
	/*public static MotTestCentre makeNewCentre(String name, int noPass, int noFail,ArrayList<MotTestCentre> all) {
		MotTestCentre res = new MotTestCentre(name,noPass,noFail);
		all.add(res);
		return res;
	}*/

}
