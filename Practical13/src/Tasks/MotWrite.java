package Tasks;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class MotWrite {
	public static void exportStats(ArrayList<MotTestCentre> myCentres, String csvOutPath) {
		try {
			PrintWriter myPw = new PrintWriter(csvOutPath);
			
			myPw.println("TestCentre, TotalCarsTested, CarsPassed, CarsFailed, PassPerc, FailPerc"); 
			
			for (MotTestCentre eachCentre : myCentres) { 
				myPw.print(eachCentre.writeToCsv());
			} 
			System.out.println("Info. successfully exported.");
			myPw.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} 
	
	public static void appendInfo(MotTestCentre newCentre, String csvPath) {
		
		FileWriter myFw;
		try {
			myFw = new FileWriter(csvPath,true);
			PrintWriter myPw = new PrintWriter(myFw);
			myPw.println();
			myPw.print(newCentre.getCentreName()+", "+newCentre.getNoPass()+", "+newCentre.getNoFail());
			
			myPw.close();
			myFw.close();
			
			System.out.println("Successfully Added.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
