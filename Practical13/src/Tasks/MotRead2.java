package Tasks;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class MotRead2 {

	public static ArrayList<MotTestCentre> importMotData(String path, boolean hasHeader) {
		// TODO Auto-generated method stub
		
		ArrayList<MotTestCentre> myCentres = new ArrayList<MotTestCentre>();
		try {
			File myF = new File(path);
			Scanner in = new Scanner(myF);
			
			if(hasHeader) {
				in.nextLine();
			}
			while(in.hasNextLine()) {
				String record = in.nextLine();
				String[] parts = record.split(",");
				
				String centreName = parts[0].trim();
				int noPassed = Integer.parseInt(parts[1].trim());
				int noFailed = Integer.parseInt(parts[2].trim());
				
				myCentres.add(new MotTestCentre(centreName,noPassed,noFailed));
			}
			
			int totalPass = 0, totalFail = 0; 
			for (MotTestCentre eachCentre : myCentres) { 
				totalPass += eachCentre.getNoPass();
				totalFail += eachCentre.getNoFail(); 
			} 
			myCentres.add(new MotTestCentre("All Mot Centres", totalPass, totalFail)); 
			in.close();
			return myCentres;
		}catch(FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

}
