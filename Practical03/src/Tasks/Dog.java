package Tasks;

public class Dog { 
	private String name, breed;
	private char gender; 
	private int age; 
	private double weight, foodBill, vetBill, petInsuranceClaim, billTotal;  
	
	public Dog(String name, String breed, char gender, int age, double weight, double vetBill)  {
		this.name = name; 
		this.breed = breed; 
		this.gender = gender; 
		this.age = age; 
		this.weight = weight;
		foodBill = 0.00;  
		this.vetBill = vetBill;  
		petInsuranceClaim = 35.50;    
		billTotal = 0.00;
	} 
	
	public double getBillTotal() {
		return billTotal;
	}
	public String getName(){  
		return name;
	}
	public String getBreed(){  
		return breed; 
	}
	public char getGender(){  
		return gender; 
	}
	public int getAge(){ 
		return age;
	}
	public double getWeight(){ 
		return weight; 
	}
	
	public void vetVisit(){  
		 vetBill = vetBill - petInsuranceClaim; 
		 System.out.println("Vet bill: " + vetBill); 
		 finalBill (); 
	} 
	public void finalBill(){ 
		billTotal = foodBill + vetBill; 
	}
	public void foodCost(int weeks, double costPerWeek){ 
		foodBill = (double) weeks * costPerWeek; 
		System.out.println("Cost of food for " + weeks + " weeks is " + foodBill); 
		finalBill(); 
	} 
	 
	public String getDogDetails() { 
		 String returnString = "-----Details of Dog-----\n"; 
		 returnString += "Name: " + this.name + " is of breed " + this.breed;
		 returnString += "\nGender: " + this.gender + "\nAge: " + this.age;   return returnString; 
	} 
} 