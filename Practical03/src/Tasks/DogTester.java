package Tasks;

public class DogTester {

	public static void main(String[] args) {
		 Dog charlie = new Dog("Charlie", "Golden Retriever", 'M', 5, 14.30, 60.00); 
		 System.out.println(charlie.getDogDetails());  
		 charlie.vetVisit();  
		 charlie.foodCost(5, 10.00); 
		 charlie.finalBill(); 
		 System.out.println("The bill total is: " + charlie.getBillTotal()); 
	}
	
}
