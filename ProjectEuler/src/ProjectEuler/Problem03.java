package ProjectEuler;
import java.math.BigInteger;
public class Problem03 {

	public static void main(String[] args) {
		//find the largest prime factor of 600851475143
		long num = 600851475143L;
		int lpf=2;
		while (num>lpf) {
			if(num%lpf==0) {
				num/=lpf;
				lpf=2;
			}else {
				lpf++;
			}
		}
		System.out.println(lpf);
	}

}
