package ProjectEuler;

import java.math.*;

public class Problem12 {

	public static void main(String[] args) {
		boolean notEnough = true;
		int num = 1;
		int toCheck = 0;
		while(notEnough) {
			int numberOfFactors = 0;
			toCheck+=num;
			for(int i=1;i<Math.sqrt(toCheck);i++) {
				if(toCheck%i==0) numberOfFactors+=2;
			}
			if(numberOfFactors>=500) {
				notEnough = false;
			}
			num++;
		}
		System.out.print(toCheck);

	}

}
