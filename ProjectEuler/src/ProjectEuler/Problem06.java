package ProjectEuler;

public class Problem06 {

	public static void main(String[] args) {
		//find the difference between the sum of 1-100 squared and 1^2+...+100^2
		long sum=0,squared=0;
		for(int i=0;i<101;i++) {
			sum+=i;
			squared+=(i*i);
		}
		sum*=sum;
		System.out.print(sum-squared);

	}

}
