package ProjectEuler;
import java.math.*;

public class Problem10 {

	public static void main(String[] args) {
		//find the sum of primes below 2million
		
		long sum=2;
		boolean prime=true;
		
		long num=1;
		while (num<2000000) {
			num+=2;
			for(long i=2;i<=Math.sqrt(num);i++) {
				prime = true;
				if(num%i==0) {
					prime = false;
					break;
				}
			}
			if(prime) {
				sum+=num;
			}
			
		
		}
		System.out.println(sum);

	}

}
