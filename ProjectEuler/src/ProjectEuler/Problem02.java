package ProjectEuler;

public class Problem02 {

	public static void main(String[] args) {
		//find the sum of the even-valued fibonacci numbers below 4million
		int a=1,b=2,c,res=0;
		do{
			if(b%2==0)
				res+=b;
			c=a+b;
			a=b;
			b=c;
			System.out.println("a: "+a+ "\tb: "+b+"\tc: "+c);
		}while(b<4000000); 
		
		System.out.println(res);
	}

}
