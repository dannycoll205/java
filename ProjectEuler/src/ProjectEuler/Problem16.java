package ProjectEuler;

import java.math.BigInteger;

public class Problem16 {

	public static void main(String[] args) {
		BigInteger bigPower = new BigInteger("2");
		bigPower = bigPower.pow(1000);
		String res = bigPower.toString();
		int sum = 0;
		for(int i=0;i<res.length();i++) {
			int toAdd = Integer.parseInt(res.substring(i,i+1));
			sum+=toAdd;
		}
		System.out.print(sum);
	}

}
