package ProjectEuler;

public class Problem01 {

	public static void main(String[] args) {
		//find the sum of all the numbers that are multiples of 3&5 below 1000
		int result=0;
		for(int i=0;i<1000;i++) {
			if(i%3==0||i%5==0)
				result+=i;
		}
		System.out.println(result);
	}

}
