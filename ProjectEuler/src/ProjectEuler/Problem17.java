package ProjectEuler;

public class Problem17 {

	public static void main(String[] args) {
		long numLetters = 0;
		String result ="";
		String one ="one"; String two ="two";String three="three";String four="four";
		String five="five"; String six ="six"; String seven="seven";String eight="eight";
		String nine="nine";
		for(int i=0;i<1001;i++) {
			if(i==1000) result+="onethousand";
			if(i>=900&&i<1000)	result+="ninehundred";
			if(i<900&&i>=800) result+="eighthundred";
			if(i<800&&i>=700) result+="sevenhundred";
			if(i<700&&i>=600) result+="sixhundred";
			if(i<600&&i>=500) result+="fivehundred";
			if(i<500&&i>=400) result+="fourhundred";
			if(i<400&&i>=300) result+="threehundred";
			if(i<300&&i>=200) result+="twohundred";
			if(i<200&&i>=100) result+="onehundred";
			if(i%100>=20&&i%100<30) result+="twenty";
			if(i%100>=30&&i%100<40) result+="thirty";
			if(i%100>=40&&i%100<50) result+="forty";
			if(i%100>=50&&i%100<60) result+="fifty";
			if(i%100>=60&&i%100<70) result+="sixty";
			if(i%100>=70&&i%100<80) result+="seventy";
			if(i%100>=80&&i%100<90) result+="eighty";
			if(i%100>=90&&i%100<100) result+="ninety";
			if(i>100&&i%100!=0) result+="and";
			if(i%100<10||i%100>20) {
				if(i%10==1) result += one;
				if(i%10==2) result+=two;
				if(i%10==3) result+=three;
				if(i%10==4) result+=four;
				if(i%10==5) result+=five;
				if(i%10==6) result+=six;
				if(i%10==7) result+=seven;
				if(i%10==8) result+=eight;
				if(i%10==9) result+=nine;
			}
			if(i%100==10)result+="ten";
			if(i%100==11) result+="eleven";
			if(i%100==12) result+="twelve";
			if(i%100==13) result+="thirteen";
			if(i%100==14) result+="fourteen";
			if(i%100==15) result+="fifteen";
			if(i%100==16) result+="sixteen";
			if(i%100==17) result+="seventeen";
			if(i%100==18) result+="eighteen";
			if(i%100==19) result+="nineteen";
			
		}
		System.out.println(result);
		System.out.println(result.length());
	}

}
