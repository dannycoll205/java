package ProjectEuler;

public class Problem14 {

	public static void main(String[] args) {
		long number;
		long longest = -1;
		long currentLength;
		long res = 0;
		for(long i=1000000;i>0;i--) {
			number = i;
			currentLength = 0;
			while(number!=1) {
				currentLength++;
				if(number%2==0) number = number/2;
				else number = (3*number)+1;
			}
			if(currentLength>longest) {
				longest = currentLength;
				res = i;
			}
		}
		
		System.out.print(res);

	}

}
